<?php require_once("intc/header.php");
    if(!isset($_COOKIE['login_userid'])) {
        header('location: 404.php');
    } else {
        $user = $db->fetchId("db_users", $_COOKIE['login_userid']);
        $result = $db->fetchSql("SELECT o.`id`, o.`transaction_id`, p.`name`, p.`images`, o.`qty`, o.`amount`, o.`created_date` FROM ( ( `db_orders` AS o INNER JOIN `db_products` AS p ON o.`product_id` = p.`id` ) INNER JOIN `db_users` AS u ON u.`id` = o.`created_by_user_id` ) WHERE o.`created_by_user_id` = {$_COOKIE['login_userid']}");
    
    }
?>
<div class="breadcrumb-wrap">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="/">Trang chủ</a></li>
            <li><span>Hồ sơ của <?= (isset($_COOKIE['login_userid'])) ? $_COOKIE['login_fullname'] : '';?></span></li>
        </ul>
    </div>
</div>
<div class="container">

    <div class="side-bar">
        <div class="box-avatar">
            <img src="<?php if(!isset($user['avatar']) || $user['avatar'] == ''){ 
            echo 'upload/avatar/noava.jpg';
        } else {
            echo $user['avatar'];
        } ?>" alt="<?= $_COOKIE['login_fullname']; ?>">
        </div>
        <div class="box-left-wrap visible-lg-block" id="boxleft-wrap">
            <div class="panel-group box-left" id="boxleft" role="tablist" aria-multiselectable="true">
                <div class="panel panel-info">
                    <div id="collapse-category" class="panel-collapse " role="tabpanel"
                        aria-labelledby="heading-category">
                        <div class="list-group">
                            <div class="list-group-item is-top">
                                <a href="ho-so"><span>Trang cá nhân</span></a>
                                <a href="lich-su-mua-hang"><span>Lịch sửa mua hàng</span></a>
                                <a href="cap-nhat-anh-dai-dien"><span>Cập nhật ảnh đại diện</span></a>
                                <a href="cap-nhat-thanh-vien"><span>Thiết lặp tài khoản</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="product-listing">
        <div class="product-box">
            <div class="option-box-wrap">
                <div class="option-box">
                    <span>Lịch sử mua hàng</span>
                </div>
            </div> <!-- end option box wrap -->
            <div class="box-pro">
                <table class="table table-width">
                    <thead >
                        <tr>
                            <th scope="col" >#</th>
                            <th scope="col" >Tên sản phẩm</th>
                            <th scope="col" >Ảnh</th>
                            <th scope="col" >Số lượng</th>
                            <th scope="col" >Tổng tiền</th>
                            <th scope="col" >Ngày mua</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($result as $item): ?>
                        <tr style="font-weight: 400">
                            <td scope="row"><?= $item['id']; ?></td>
                            <td scope="row"><?= $item['name']; ?></td>
                            <td scope="row"><img src="<?= $item['images']; ?>" alt="<?= $item['name']; ?>" width="90" /></th>
                            <td scope="row"><?= bsVndDot($item['qty']); ?></th>
                            <td scope="row"><?= $item['amount']; ?></th>
                            <td scope="row"><?= date("d/m/Y",$item['created_date']); ?></h>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php require_once("intc/footer.php"); ?>