<?php require_once("intc/header.php");
    if(!isset($_COOKIE['login_userid'])) {
        header('location: 404.php');
    } else {
        $result = $db->fetchId("db_users", $_COOKIE['login_userid']);
    }
?>
<div class="breadcrumb-wrap">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="/">Trang chủ</a></li>
            <li><span>Hồ sơ của <?= (isset($_COOKIE['login_userid'])) ? $_COOKIE['login_fullname'] : '';?></span></li>
        </ul>
    </div>
</div>
<div class="container">

    <div class="side-bar">
        <div class="box-avatar">
        <img src="<?php if(!isset($result['avatar']) || $result['avatar'] == ''){ 
            echo 'upload/avatar/noava.jpg';
        } else {
            echo $result['avatar'];
        } ?>" alt="<?= $_COOKIE['login_fullname']; ?>">
        </div>
        <div class="box-left-wrap visible-lg-block" id="boxleft-wrap">
            <div class="panel-group box-left" id="boxleft" role="tablist" aria-multiselectable="true">
                <div class="panel panel-info">
                    <div id="collapse-category" class="panel-collapse " role="tabpanel"
                        aria-labelledby="heading-category">
                        <div class="list-group">
                            <div class="list-group-item is-top">
                                <a href="ho-so"><span>Trang cá nhân</span></a>
                                <a href="lich-su-mua-hang"><span>Lịch sửa mua hàng</span></a>
                                <a href="cap-nhat-anh-dai-dien"><span>Cập nhật ảnh đại diện</span></a>
                                <a href="cap-nhat-thanh-vien"><span>Thiết lặp tài khoản</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="product-listing">
        <div class="product-box">
            <div class="option-box-wrap">
                <div class="option-box">
                    <span>Thông Tin Cá Nhân</span>
                </div>
            </div> <!-- end option box wrap -->
            <div class="box-pro">
                <table class="table" style="width: 100%">
                    <tbody>
                        <tr>
                            <th scope="col">Họ Tên</th>
                            <td scope="row"><span class="fw"><?= $result['full_name']; ?></span></td>
                        </tr><span>
                        <tr>
                            <th scope="col">Tài Khoản</th>
                            <td scope="row"><span class="fw"><?= $result['user_name']; ?></span></td>
                        </tr><span>
                        <tr>
                            <th scope="col">Email</th>
                            <td scope="row"><span class="fw"><?= $result['email']; ?></span></td>
                        </tr><span>
                        <tr>
                            <th scope="col">Địa Chỉ</th>
                            <td scope="row"><span class="fw"><?= $result['address']; ?></span></td>
                        </tr><span>
                        <tr>
                            <th scope="col">Điện Thoại</th>
                            <td scope="row"><span class="fw"><?= $result['phone']; ?></span></td>
                        </tr><span>
                        <tr>
                            <th scope="col">Giới Tính</th>
                            <td scope="row"><span class="fw"><?= ($result['gender'] == 1) ? 'Nam' : 'Nữ'; ?></span></td>
                        </tr><span>
                        <tr>
                            <th scope="col">Trạng Thái</th>
                            <td scope="row"><span class="fw"><?= ($result['status'] == 1) ? 'Hoạt động' : 'Bị Khóa'; ?></span></td>
                        </tr><span>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php require_once("intc/footer.php"); ?>