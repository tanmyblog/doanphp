<?php require_once("intc/header.php");
    if(!isset($_COOKIE['login_userid'])) {
        header('location: 404.php');
    } else {
        $result = $db->fetchId("db_users", $_COOKIE['login_userid']);
        if(isset($_POST['updateuser'])) {
            if(isset($_POST['editpass'])) {
                $data = array(
                    'user_name' => mysqli_real_escape_string($db->conn, trim($_POST['editusername'])),
                    'pass_word' => md5(mysqli_real_escape_string($db->conn, trim($_POST['editpass']))),
                    'full_name' => mysqli_real_escape_string($db->conn, trim($_POST['editfullname'])),
                    'email'     => mysqli_real_escape_string($db->conn, trim($_POST['editemail'])),
                    'gender'    => $_POST['gender'],
                    'address'   => mysqli_real_escape_string($db->conn, trim($_POST['editaddress'])),
                    'phone'     => mysqli_real_escape_string($db->conn, trim($_POST['editphone'])),
                    'status'    => $_POST['status']
                );
            } else {
                $data = array(
                    'user_name' => mysqli_real_escape_string($db->conn, trim($_POST['editusername'])),
                    'full_name' => mysqli_real_escape_string($db->conn, trim($_POST['editfullname'])),
                    'email'     => mysqli_real_escape_string($db->conn, trim($_POST['editemail'])),
                    'gender'    => $_POST['gender'],
                    'address'   => mysqli_real_escape_string($db->conn, trim($_POST['editaddress'])),
                    'phone'     => mysqli_real_escape_string($db->conn, trim($_POST['editphone'])),
                    'status'    => $_POST['status']
                );
            }
            if($db->update("db_users", $data, array('id' => $_COOKIE['login_userid']))) {
                header('location: profile.php');
            } else {
                $error_edit = "Lỗi không thể cập nhật";
            }
        }
    }
?>
<div class="breadcrumb-wrap">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="/">Trang chủ</a></li>
            <li><span>Hồ sơ của <?= (isset($_COOKIE['login_userid'])) ? $_COOKIE['login_fullname'] : '';?></span></li>
        </ul>
    </div>
</div>
<div class="container">

    <div class="side-bar">
        <div class="box-avatar">
        <img src="<?php if(!isset($result['avatar']) || $result['avatar'] == ''){ 
            echo 'upload/avatar/noava.jpg';
        } else {
            echo $result['avatar'];
        } ?>" alt="<?= $_COOKIE['login_fullname']; ?>">
        </div>
        <div class="box-left-wrap visible-lg-block" id="boxleft-wrap">
            <div class="panel-group box-left" id="boxleft" role="tablist" aria-multiselectable="true">
                <div class="panel panel-info">
                    <div id="collapse-category" class="panel-collapse " role="tabpanel"
                        aria-labelledby="heading-category">
                        <div class="list-group">
                            <div class="list-group-item is-top">
                                <a href="ho-so"><span>Trang cá nhân</span></a>
                                <a href="lich-su-mua-hang"><span>Lịch sửa mua hàng</span></a>
                                <a href="cap-nhat-anh-dai-dien"><span>Cập nhật ảnh đại diện</span></a>
                                <a href="cap-nhat-thanh-vien"><span>Thiết lặp tài khoản</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="product-listing">
        <div class="product-box">
            <div class="option-box-wrap">
                <div class="option-box">
                    <span>Thông Tin Cá Nhân</span>
                </div>
            </div> <!-- end option box wrap -->
            <div class="box-pro">
                <form method="post" action="">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="editfullname">Họ Tên</label>
                            <input type="text" class="form-control" id="editfullname" name="editfullname" placeholder="Họ Tên" value="<?= $result['full_name'] ?>">
                            <label for="gender" id="editfullname_error" style="color: red;"></label>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="editusername">Tài Khoản</label>
                            <input type="text" class="form-control" id="editusername" name="editusername" placeholder="Tài khoản" value="<?= $result['user_name'] ?>">
                            <label for="gender" id="editusername_error" style="color: red;"></label>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="editemail">Email</label>
                            <input type="email" class="form-control" id="editemail" name="editemail" placeholder="Email" value="<?= $result['email'] ?>">
                            <label for="gender" id="editemail_error" style="color: red;"></label>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="editpass">Password</label>
                            <input type="password" class="form-control" id="editpass" name="editpass" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="editaddress">Địa Chỉ</label>
                            <input type="text" class="form-control" id="editaddress" name="editaddress" placeholder="Địa chỉ" value="<?= $result['address'] ?>">
                            <label for="gender" id="editaddress_error" style="color: red;"></label>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="editphone">Điện thoại</label>
                            <input type="text" class="form-control" id="editphone" name="editphone" placeholder="Điện thoại" value="<?= $result['phone'] ?>">
                            <label for="gender" id="editphone_error" style="color: red;"></label>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="gender1" name="gender" class="custom-control-input" value="1" <?= ($result['gender'] == 1) ? 'checked' : ''; ?>>
                                <label class="custom-control-label" for="gender1">Nam</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="gender2" name="gender" class="custom-control-input" value="0" value="1" <?= ($result['gender'] == 0) ? 'checked' : ''; ?>>
                                <label class="custom-control-label" for="gender2">Nữ</label>
                            </div>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="status" value="1" value="1" <?= ($result['status'] == 1) ? 'checked' : ''; ?>>
                            <label class="custom-control-label" for="status">Bật / Tắt</label>
                        </div>
                    </div>
                    
                    <button type="submit" name="updateuser" onclick="return checkvalidate(); " class="btn btn-primary">Cập Nhật</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function checkvalidate() {
    $('#editfullname_error').hide();
    $('#editusername_error').hide();
    $('#editemail_error').hide();
    $('#editaddress_error').hide();
    $('#editphone_error').hide();

    var editfullname_error = false;
    var editusername_error = false;
    var editemail_error = false;
    var editaddress_error = false;
    var editphone_error = false;

    if ($('#editfullname').val() == '') {
        $('#editfullname_error').html('Họ tên rỗng !');
        $('#editfullname_error').show();
        editfullname_error = true;
        return false;
    } else if ($('#editusername').val() == '') {
        $('#editusername_error').html('Tên đăng nhập rỗng !');
        $('#editusername_error').show();
        editusername_error = true;
        return false;
    } else if ($('#editemail').val() == '' || !validateEmail($('#editemail').val())) {
        $('#editemail_error').html('Email không hợp lệ !');
        $('#editemail_error').show();
        editemail_error = true;
        return false;
    } else if ($('#editaddress').val() == '') {
        $('#editaddress_error').html('Mật khẩu rỗng !');
        $('#editaddress_error').show();
        editaddress_error = true;
        return false;
    } else if ($('#editphone').val() == '' || ($('#editphone').val().length > 12) || !validatePhone($('#editphone').val())) {
        $('#editphone_error').html('Số điện thoại không hợp lệ !');
        $('#editphone_error').show();
        phone_error = true;
        return false;
    } 
    return true;
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function validatePhone(phone) {
    var intRegex = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
    return intRegex.test(phone);
}
</script>
<?php require_once("intc/footer.php"); ?>