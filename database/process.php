<?php session_start(); ob_start();
    require_once("database.php");
    include("../admin/helper/common_helper.php");
    $db = new Database();


    // Thêm sữa xóa danh mục tài khoản 
    if(isset($_POST['addUser'])) {
        $data = array(
            'user_name'         => $_POST['username'],
            'pass_word'         => md5($_POST['password']),
            'full_name'         => $_POST['fullname'],
            'email'             => $_POST['email'],
            'avatar'            => $_POST['avata'], 
            'note'              => $_POST['note'],
            'sort'              => $_POST['sort'],
            'created_date'      => time(),
            'status'            => isset($_POST['status']) ? $_POST['status'] : 1
        );

        $result = $db->insert("db_admins", $data);
        if($result) {
            header("location: ../admin/account.php");
        } else {
            echo "Error: ". mysqli_error($db);
        }
        exit();
    }
    if(isset($_POST['editUser'])) {
        if(isset($_POST['password']) && $_POST['password'] != ""){
            $data = array(
                'user_name'         => $_POST['username'],
                'pass_word'         => md5($_POST['password']),
                'full_name'         => $_POST['fullname'],
                'email'             => $_POST['email'],
                'avatar'            => $_POST['avata'], 
                'note'              => $_POST['note'],
                'sort'              => $_POST['sort'],
                'created_date'      => time(),
                'status'            => isset($_POST['status']) ? $_POST['status'] : 1
            );
        }else{
            $data = array(
                'user_name'         => $_POST['username'],
                'full_name'         => $_POST['fullname'],
                'email'             => $_POST['email'],
                'avatar'            => $_POST['avata'], 
                'note'              => $_POST['note'],
                'sort'              => $_POST['sort'],
                'created_date'      => time(),
                'status'            => isset($_POST['status']) ? $_POST['status'] : 1
            );
        }
        
        $id = $_POST['id'];
        $result = $db->update("db_admins", $data, array('id'=>$id));
        if($result) {
            header("location: ../admin/account.php");
        } else {
            echo "Error: ". mysqli_error($db->conn);
        }
        exit();
    }
    if(isset($_GET['ac']) && ($_GET['ac']) === "delete") {
        $id = $_GET['distDel'];
        $result = $db->delete("db_users",$_GET['id']);
        if($result) {
            header("location: ../admin/account.php");
        } else {
            echo "Error: ". mysqli_error($db);
        }
        exit();
    }

    // Thêm sửa xóa danh mục sản phẩm   
    if(isset($_POST['addCat'])) {
        $alias = strtoseo($_POST['name']);
        $data = array(
            'parent_id'     => $_POST['parent_id'],
            'name'          => $_POST['name'],
            'alias'         => $alias,
            'meta_keywords' => $_POST['meta_keywords'],
            'meta_descriptions' => $_POST['meta_descriptions'],
            'sort'              => $_POST['sort'],
            'created_by_user_id'=> $_POST['userid'],
            'created_date'      => time(),
            'status'            => isset($_POST['status']) ? $_POST['status'] : 0
        );

        $result = $db->insert("db_categories", $data);
        if($result) {
            header("location: ../admin/categorylist.php");
        } else {
            echo "Error: ". mysqli_error($db);
        }
        exit();
    } else if(isset($_POST['upCat'])) {
        $alias = strtoseo($_POST['name']);
        $data = array(
            'parent_id'     => $_POST['parent_id'],
            'name'          => $_POST['name'],
            'alias'         => $alias,
            'meta_keywords' => $_POST['meta_keywords'],
            'meta_descriptions' => $_POST['meta_descriptions'],
            'sort'              => $_POST['sort'],
            'created_by_user_id'=> $_POST['userid'],
            'status'            => isset($_POST['status']) ? $_POST['status'] : 0
        );
        $id = $_POST['idcat'];
        $result = $db->update("db_categories", $data, array('id'=>$id));
        if($result) {
            header("location: ../admin/categorylist.php");
        } else {
            echo "Error: ". mysqli_error($db);
        }
        exit();
    } else if(isset($_GET['catDel'])) {
        $result = $db->delete("db_categories",$_GET['catDel']);
        if($result) {
            header("location: ../admin/categorylist.php");
        } else {
            echo "Error: ". mysqli_error($db);
        }
        exit();
    }

    // Thêm sửa xóa sản phẩm
    if(isset($_POST['addPro'])) {
        $alias = strtoseo($_POST['name']);
        if(isset($_POST['sale_price'])) {
            $sale_price = (int)str_replace(".", "",$_POST['sale_price']);
        } else {$sale_price = (int)0;}

        if (isset($_FILES['uf'])) {
            $target="../upload/";
            $filename=basename($_FILES['uf']['name']);
            $target.=$filename;
            $link="upload/".$filename;
            if (file_exists($target)) {
                echo "<script>alert('Ảnh đã tồn tại');</script>";
            }

            if(preg_match("/\.(jpeg2wbmp(jpegname, wbmpname, dest_height, dest_width, threshold)|png|bmp|gif)$/i",basename($_FILES['uf']['name']))) {
                if (move_uploaded_file($_FILES['uf']['tmp_name'], $target)) {
                    $data = array(
                        'category_id'       => $_POST['category_id'],
                        'product_code'      => $_POST['product_code'],
                        'name'              => $_POST['name'],
                        'alias'             => $alias,
                        'images'            => $link,
                        'shot_descriptions' => $_POST['shot_descriptions'],
                        'contents'          => $_POST['editor1'],
                        'meta_keywords'     => $_POST['meta_keywords'],
                        'meta_descriptions' => $_POST['meta_descriptions'],
                        'price'             => (int)str_replace(".", "",$_POST['price']),
                        'sale_price'        => $sale_price,
                        'qty'               => (int)str_replace(".", "",$_POST['qty']),
                        'buyed'             => 0,
                        'view_count'        => 0,
                        'created_by_user_id'=> $_POST['userid'],
                        'created_date'      => time(),
                        'updated_date'      => time(),
                        'hot'               => isset($_POST['hot']) ? $_POST['hot'] : 0,
                        'new'               => isset($_POST['new']) ? $_POST['new'] : 0,
                        'trash'             => 0,
                        'status'            => isset($_POST['status']) ? $_POST['status'] : 0
                    );
                    // echo "<pre>".print_r($data)."</pre>";

                    $result = $db->insert("db_products", $data);
                    if($result) {
                        header("location: ../admin/productlist.php");
                    } else {
                        echo "Error: ". mysqli_error($db);
                    }
                }
            }
        }
    } else if(isset($_POST['upPro'])){
        $alias = strtoseo($_POST['name']);
        if(isset($_POST['sale_price'])) {
            $sale_price = (int)str_replace(".", "",$_POST['sale_price']);
        } else {$sale_price = (int)0;}
        $id = $_POST['idpro'];

        if (isset($_FILES['uf']) && $_FILES['uf']['name']!="") {
            $target="../upload/";
            $filename=basename($_FILES['uf']['name']);
            $target.=$filename;
            $link="upload/".$filename;
            if (file_exists($target)) {
                echo "<script>alert('Ảnh đã tồn tại');</script>";
            }

            if(preg_match("/\.(jpg|png|bmp|gif)$/i",basename($_FILES['uf']['name']))) {
                if (move_uploaded_file($_FILES['uf']['tmp_name'], $target)) {
                    $data = array(
                        'category_id'       => $_POST['category_id'],
                        'product_code'      => $_POST['product_code'],
                        'name'              => $_POST['name'],
                        'alias'             => $alias,
                        'images'            => $link,
                        'shot_descriptions' => $_POST['shot_descriptions'],
                        'contents'          => $_POST['editor1'],
                        'meta_keywords'     => $_POST['meta_keywords'],
                        'meta_descriptions' => $_POST['meta_descriptions'],
                        'price'             => (int)str_replace(".", "",$_POST['price']),
                        'sale_price'        => $sale_price,
                        'qty'               => (int)str_replace(".", "",$_POST['qty']),
                        'buyed'             => 0,
                        'view_count'        => 0,
                        'created_by_user_id'=> $_POST['userid'],
                        'created_date'      => time(),
                        'updated_date'      => time(),
                        'hot'               => isset($_POST['hot']) ? $_POST['hot'] : 0,
                        'new'               => isset($_POST['new']) ? $_POST['new'] : 0,
                        'trash'             => 0,
                        'status'            => isset($_POST['status']) ? $_POST['status'] : 0
                    );
                    // echo "<pre>".print_r($data)."</pre>";
                    
                    $result = $db->update("db_products", $data, array('id'=>$id));
                    if($result) {
                        header("location: ../admin/productlist.php");
                    } else {
                        echo "Error: ". mysqli_error($db);
                    }
                }
            }
        } 
        else { 
            // Không có thay đổi hình
            $data = array(
                'category_id'       => $_POST['category_id'],
                'product_code'      => $_POST['product_code'],
                'name'              => $_POST['name'],
                'alias'             => $alias,
                'shot_descriptions' => $_POST['shot_descriptions'],
                'contents'          => $_POST['editor1'],
                'meta_keywords'     => $_POST['meta_keywords'],
                'meta_descriptions' => $_POST['meta_descriptions'],
                'price'             => (int)str_replace(".", "",$_POST['price']),
                'sale_price'        => $sale_price,
                'qty'               => (int)str_replace(".", "",$_POST['qty']),
                'buyed'             => 0,
                'view_count'        => 0,
                'created_by_user_id'=> $_POST['userid'],
                'created_date'      => time(),
                'updated_date'      => time(),
                'hot'               => isset($_POST['hot']) ? $_POST['hot'] : 0,
                'new'               => isset($_POST['new']) ? $_POST['new'] : 0,
                'trash'             => 0,
                'status'            => isset($_POST['status']) ? $_POST['status'] : 0
            );
            $result = $db->update("db_products", $data, array('id'=>$id));
            if($result) {
                header("location: ../admin/productlist.php");
            } else {
                echo "Error: ". mysqli_error($db);
            }
        }
    } else if(isset($_GET['proDel'])) {
        $result = $db->delete("db_products",$_GET['proDel']);
        if($result) {
            header("location: ../admin/productlist.php");
        } else {
            echo "Error: ". mysqli_error($db);
        }
        exit();
    }

    // Xóa slide
    if(isset($_GET['slideDel'])) {
        $result = $db->delete("db_slides",$_GET['slideDel']);
        if($result) {
            header("location: ../admin/slidelist.php");
        } else {
            echo "Error: ". mysqli_error($db);
        }
        exit();
    }
    
    // Thêm sửa xóa quận huyện
    if(isset($_POST['addProvin'])) {
        $data = array(
            'name'  => $_POST['name'],
            'sort'  => $_POST['sort'],
            'status'=> isset($_POST['status']) ? $_POST['status'] : 0
        );

        $result = $db->insert("db_provinces", $data);
        if($result) {
            header("location: ../admin/provincelist.php");
        } else {
            echo "Error: ". mysqli_error($db);
        }
        exit();
    } else if(isset($_GET['provinDel'])) {
        $id = $_GET['provinDel'];
        $result = $db->delete("db_provinces",$_GET['provinDel']);
        if($result) {
            header("location: ../admin/provincelist.php");
        } else {
            echo "Error: ". mysqli_error($db);
        }
        exit();
    }

    // Thêm sửa xóa tỉnh thành phố
    if(isset($_POST['addDist'])) {
        $data = array(
            'name'  => $_POST['name'],
            'provin_id' => $_POST['provin_id'],
            'sort'  => $_POST['sort'],
            'status'=> isset($_POST['status']) ? $_POST['status'] : 0
        );

        $result = $db->insert("db_districts", $data);
        if($result) {
            header("location: ../admin/districtlist.php");
        } else {
            echo "Error: ". mysqli_error($db);
        }
        exit();
    } else if(isset($_GET['distDel'])) {
        $id = $_GET['distDel'];
        $result = $db->delete("db_districts",$_GET['distDel']);
        if($result) {
            header("location: ../admin/districtlist.php");
        } else {
            echo "Error: ". mysqli_error($db);
        }
        exit();
    }

    //  dat hang
    if(isset($_POST['btncheckout']))
    {
        // (1) Thêm vào bảng transaction
        $data = array(
            'user_id'       => $_COOKIE['login_userid'],
            'amount'        => $_POST['total'],
            'message'       => $_POST['note'],
            'created_date'  => time(),
            'status'        => 1
        );
        
        $tranId = $db->insert("db_transactions", $data);

        // (2) Thêm vào bảng order
        if($tranId > 0) {
            foreach($_SESSION['cart'] as $key => $value)
			{
				$data2 = array(
                    'transaction_id'	=> $tranId,
					'product_id'		=> $key,
					'qty'               => $value['qty'],
                    'amount'            => $value['price'],
                    'created_date'      => time(),
                    'status'            => 1
                );
                $id_insert = $db->insert("db_orders", $data2);
                
                // (3) cập nhật lại bảng product
                if($id_insert > 0) {
                    $proid = $db->fetchID("db_products", $key);
                    
                    foreach($_SESSION['cart'] as $k => $v)
			        {
                        // $qty = $proid['qty'] - $v['qty'];
                        // $buyed = $proid['buyed'] + $v['qty'];

                        $data3 = array(
                            'qty'   => $proid['qty'] - $v['qty'],
                            'buyed' => $proid['buyed'] + $v['qty']
                        );
                    }
                    $id = $proid['id'];
                    
                    // $uppro = $db->update("db_products", $data3, array('id' => $key) );
                    $result = $db->update("db_products", $data3, array('id'=>$id));
                }
			}

			unset($_SESSION['cart']);
            unset($_SESSION['totalPrice']);
            
            $_SESSION['success'] = "Đặt hàng thành công! chúng tôi sẽ liên hệ bạn sớm nhất.";
			header("location: ../checkout.php");
        }
    }
    
    // xao gio hang 
    if(isset($_GET['rmcart'])) {
        $id = (int)$_GET['rmcart'];

        unset($_SESSION['cart'][$id]);

        $_SESSION['rm_sucess'] = "Đã xóa sản phẩm ra khỏi giỏ hàng";
        header("location: ../checkout.php");
    }

?>