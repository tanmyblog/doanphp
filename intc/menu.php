    <div class="clear-both"></div>

<div class="gird-head">
    <div class="grid slidehead">
        <span class="btn-menu-toggle">
            <span class="line line1"></span>
            <span class="line"></span>
            <span class="line line3"></span>
        </span>
        <div class="div_logo">
            <a href="<?= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]"; ?>"><img src="source/images/logo.png" border="0" alt="Logo đại diện website bán lan" /></a>
        </div>
        <div class="list_menu_right">
            <form action="search.php" method="get" >
            <div class="search">
                <input type="text" id="search_input" name="keyword" class="input_search" placeholder="Tìm kiếm sản phẩm" value="<?= (isset($_GET['keyword'])) ? $_GET['keyword'] : ''; ?>">
                <input type="hidden" name="catId" id="s-catelog" value="0">
                <div class="selectbox">
                    <span class="html-selected">Tất cả danh mục <i class="fa fa-caret-down"></i></span>
                    <span style="position:relative; float:left;">
                        <?php $result = $db->fetchSql("SELECT `id`, `name`, `alias` FROM `db_categories` WHERE `parent_id` = 0 "); ?>
                        <ul class="ul-selected">
                            <li class="selected">Tất cả danh mục</li>
                            <?php foreach($result as $item): ?>
                                <?php $sub = $db->fetchSql("SELECT `id`, `name`, `alias` FROM `db_categories` WHERE `parent_id` = {$item['id']} "); ?>
                                <li value="<?= $item['id']; ?>" style="font-weight: bold"><?= $item['name']; ?></li>
                                <?php if(!empty($sub)):
                                    foreach($sub as $row): 
                                ?>
                                <li value="<?= $item['id']; ?>" style="margin-left: 10px;"> -- <?= $row['name']; ?></li>
                                <?php endforeach; endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </span>
                </div>
                <button type="submit" class="btn_search" id="btn_search"><i class="fa fa-search"></i></button>
            </div>
            </form>
        </div>
        <div class="div_top_right">
            <div class="box-info-cart" onClick="window.location='/dat-hang'">
                <i class="fa fa-shopping-cart"></i>
                <?php  $countCart = 0; 
                    if(isset($_SESSION['cart'])) {
                    foreach($_SESSION['cart'] as $key => $value) {
                        $countCart += $value['qty'];
                    }
                } ?>
                <span class="count-cart"><?= $countCart; ?></span>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div class="clear-both"></div>

<div class="header_main">
    <div class="header-container">
        <div class="menu_tap">
            <nav class="menu"><i class="fa fa-bars"></i>CHỌN DANH MỤC</nav>
            <div class="menu-hw">
                <ul class="menu_left_ul">
                    <?php $catlist = $db->fetchSql("SELECT `id`, `name`, `alias` FROM `db_categories` WHERE `parent_id` = 0 ORDER BY `name`"); ?>
                    <?php foreach($catlist as $item): ?>
                    <li>
                        <a href="<?= $item['alias'] ?>-<?php echo $item['id']; ?>" class="arrow">
                            <?php echo $item['name']; ?><span class="white-line"></span></a>
                        <?php $subcat = $db->fetchSql("SELECT `id`, `name`, `alias` FROM `db_categories` WHERE `parent_id` = {$item['id']} ORDER BY `name`"); ?>
                        <?php if($subcat): ?>
                        <div class="sub-cate">
                            <div class="sub-cate-inner">
                                <ul>
                                    <?php foreach($subcat as $row): ?>
                                    <li><a href="<?= $row['alias'] ?>-<?php echo $row['id']; ?>" class="title"><?= $row['name']; ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                        <?php endif; ?>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div class="menu-horizontal">
            <ul>
                <li><a href="/xu-huong.html">Xu hướng</a></li>
                <li><a href="/thuong-hieu.html">Thương hiệu </a></li>
                <li><a href="/deal-gia-re.html">DEAL </a></li>
                <li><a href="/">Sự kiện</a></li>
            </ul>
        </div>

    </div>
</div>

<div class="slide-bar-menu">
    <div class="nav-search-menu search-bar">
        <form action="/tim-kiem.html" method="get">
            <input type="hidden" name="run" value="1">
            <div class="search-input-select">
                <input type="search" class="input-search" name="s" id="search-menu-left" placeholder="Tìm kiếm sản phẩm"
                    maxlength="128">
            </div>
            <div class="search-btn">
                <button class="ico-mobile" title="" type="submit"><i class="fa fa-search"></i></button>
            </div>
        </form>
    </div>

    <nav>
        <ul class="menu-mobile">
            <li><a href="<?= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]"; ?>"><i class="icono izquierda fa fa-home"></i> Trang chủ</a></li>
            <li><a href="checkout.php"><i class="icono izquierda fas fa-shopping-cart"></i> Giỏ hàng</a></li>
            <?php foreach($catlist as $item): ?>
            <li><a href="<?= $item['alias']; ?>-<?= $item['id']; ?>"><i class="icono izquierda fas fa-dot-circle"></i> <?= $item['name']; ?> <i class="icono derecha fa fa-chevron-down"></i></a>
                <?php $subcat = $db->fetchSql("SELECT `id`, `name`, `alias` FROM `db_categories` WHERE `parent_id` = {$item['id']} ORDER BY `name`"); ?>
                <?php if($subcat): ?>
                <ul>
                    <?php foreach($subcat as $row): ?>
                    <li><a href="<?= $row['alias']; ?>-<?= $row['id']; ?>"><?= $row['name']; ?></a></li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </li>
            <?php endforeach; ?>

        </ul>
    </nav>
</div>

<div class="clear-both"></div>

<div class="overlay-open-menu"></div>

<div class="clear-both"></div>