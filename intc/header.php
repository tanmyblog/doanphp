<?php session_start(); ob_start();
    require_once('database/database.php');
    require('admin/helper/common_helper.php');
    $db = new Database();

    if(isset($_POST['userlogin'])) {

        $uname = mysqli_real_escape_string($db->conn, trim($_POST['uname']));
        $pass = mysqli_real_escape_string($db->conn, trim($_POST['pass']));

        $sql = "`user_name` = '" .$uname. "' AND `pass_word` = md5('" .$pass. "')";
        $result = $db->fetchOne("db_users",$sql);
        $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        if(!$result) {
            echo "<script> alert('Tài khoản hoặc mật khẩu không đúng. Vui lòng nhập lại'); </script>";
        } else {
            // thiết lặp thời gian đăng nhập trong 1h
            setcookie('login_userid', $result['id'], time() + 3600);
            setcookie('login_fullname', $result['full_name'], time() + 3600);
            header('location: '.$link);
        }
    } else if(isset($_POST['signup'])) {
        if($db->fetchSql("SELECT `user_name` FROM `db_users` WHERE `user_name` = '{$_POST['username']}' ")) {
            $error_signup = "Tên tài khoản đã tồn tại";
        } else {
            $data = array(
                'user_name'     => mysqli_real_escape_string($db->conn, trim($_POST['username'])),
                'pass_word'     => md5(mysqli_real_escape_string($db->conn, trim($_POST['password']))),
                'full_name'     => mysqli_real_escape_string($db->conn, trim($_POST['fullname'])),
                'email'         => mysqli_real_escape_string($db->conn, trim($_POST['email'])),
                'gender'        => $_POST['gender'],
                'address'       => mysqli_real_escape_string($db->conn, trim($_POST['address'])),
                'phone'         => mysqli_real_escape_string($db->conn, trim($_POST['phone'])),
                'created_date'  => time(),
                'status'        => 1
            );
            if($db->insert("db_users", $data)) {
                $result = $db->fetchOne("db_users", " `user_name` = '{$_POST['username']}' " );
                setcookie('login_userid', $result['id'], time() + 3600);
                setcookie('login_fullname', $result['full_name'], time() + 3600);
                header('location: /');
            } else {
                echo "Error: ". mysqli_error($db);
            }
        }
    }

    
?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <base href="<?= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";?>">
    <meta charset="UTF-8">
    <title>Website bán lan và Phụ kiện lan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="source/images/favicon.png" rel="shortcut icon" type="image/x-icon">
    <link rel="stylesheet" href="source/css/bootstrap.min.css">
    <link rel="stylesheet" href="source/css/jquery-ui.min.css">
    <link rel="stylesheet" href="source/css/all.min.css">
    <link rel="stylesheet" href="source/css/flexslider.css">
    <link rel="stylesheet" href="source/css/owl.theme.css">
    <link rel="stylesheet" href="source/css/owl.carousel.css">
    <link rel="stylesheet" href="source/css/style.css">
</head>

<body>
    <div class="headbar">
        <div class="grid">
            <ul>
                <li class="hotline"><a href="tel:0964082598">Hotline: <strong>0964082598</strong></a></li>
                <?php if(!isset($_COOKIE['login_userid'])) {
                    ?>
                <li><button data-modal-trigger="trigger-1" class="trigger">Đăng nhập</button></li>
                <?php } else { ?>
                <li class="dropdown-login"> <?= isset($_COOKIE['login_fullname']) ? $_COOKIE['login_fullname'] : ''; ?> <i class="fas fa-caret-down"></i>
                    <ul class="login-sub">
                        <li><a href="/ho-so">Tài khoản của tôi</a></li>
                        <li>
                            <form action="logout.php" method="post" style="margin-left: 5px;">
                                <input type="hidden" name="hidden" value="1">
                                <button type="submit" name="logout_user" class="btn">Đăng xuất</button>
                            </form>
                        </li>
                    </ul>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
 
    <?php require_once("menu.php"); ?>