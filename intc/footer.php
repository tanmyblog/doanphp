<div class="footer_info">
        <div class="container">
            <div class="logo_footer">
                <div class="footer-bottom-box">
                    <h3>Review</h3>
                    <ul>
                        <li><a href="/tu-van/dong-ho-sinh-vien-gia-re">Đồng hồ sinh viên giá rẻ</a></li>
                        <li><a href="/tu-van/giang-sinh-co-y-nghia-gi-tai-cac-nuoc">Giáng sinh có ý nghĩa gì tại các
                                nước</a></li>
                        <li><a href="/tu-van/ngay-online-friday-giam-gia-sieu-khung">Ngày Online Friday giảm giá siêu
                                khủng</a></li>
                        <li><a href="/tu-van/y-nghia-va-cach-an-tet-nguyen-dan-cua-nguoi-viet">Ý nghĩa và cách ăn Tết
                                Nguyên Đán của người Việt</a></li>
                        <li><a href="/tu-van/do-chiu-nuoc-3atm-nhu-the-nao">Độ chịu nước 3ATM như thế nào?</a></li>
                    </ul>
                </div>
                <div class="footer-bottom-box">
                    <h3>Về Chúng Tôi</h3>
                    <ul>
                        <li><a href="/tu-van/huong-dan-mua-hang-truc-tuyen">Hướng dẫn mua hàng trực tuyến</a></li>
                        <li><a href="/tu-van/ve-chung-toi">Về chúng tôi</a></li>
                        <li><a href="/tu-van/tuyen-dung">Tuyển dụng</a></li>
                        <li><a href="/tu-van/lien-he">Liên Hệ</a></li>
                    </ul>
                </div>
                <div class="footer-bottom-box">
                    <h3>Dành Cho Người Mua Hàng</h3>
                    <ul>
                        <li><a href="/tu-van/chinh-sach-doi-tra-hang-hoan-tien">Chính sách đổi trả hàng - Hoàn Tiền</a></li>
                        <li><a href="/tu-van/chinh-sach-van-chuyen">Chính sách vận chuyển</a></li>
                        <li><a href="/tu-van/chinh-sach-bao-hanh">Chính Sách Bảo Hành</a></li>
                        <li><a href="/tu-van/trung-tam-bao-hanh">Trung Tâm Bảo Hành</a></li>
                    </ul>
                </div>
            </div>
            <div class="colfoo4">
                <h3 class="title_3IrN">Đăng ký nhận khuyến mãi</h3>
                <div class="subscribeEmailBox">
                    <form class="">
                        <div class="inputEmail">
                            <input placeholder="Email của bạn là" aria-label="Đăng ký nhận bản tin ưu đãi" type="text"
                                class="form-control" value="">
                            <div class="input-group-append"><button class="btn btn-primary">Đăng ký</button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="footerLegal">
        <div class="container">
            <div id="copyright">
                <p style="text-align:center"><span></a>&nbsp; &nbsp;&copy; 2019 Được thiết kế bởi Tấn Mỹ</span></p>
            </div>
        </div>
    </div>

    <a href="#0" class="cd-top js-cd-top">Top</a>

    <?php require_once("account.php"); ?>

    <script type="text/javascript" src="source/js/jquery.min.js"></script>
    <script type="text/javascript" src="source/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="source/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="source/js/popper.min.js"></script>
    <script type="text/javascript" src="source/js/website.js"></script>
    <script type="text/javascript" src="source/js/mmenu.js"></script>
    <script type="text/javascript" src="source/js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="source/js/owl.carousel.js"></script>
    <script>
        var owl;

        function customPager() {

            $.each(this.owl.userItems, function (i) {

                var titleData = jQuery(this).find('h3').text();
                var paginationLinks = jQuery('.owl-controls .owl-pagination .owl-page span');

            });
        }

        $('.homeCarousel').owlCarousel({
            autoPlay: 3000,
            navigation: false,
            slideSpeed: 500,
            paginationSpeed: 1000,
            singleItem: true,
            afterInit: customPager,
            afterUpdate: customPager,
            navigationText: false
        });
    </script>
</body>
</html>