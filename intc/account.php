<div data-modal="trigger-1" class="modal">
    <article class="content-wrapper">
        <button class="close"></button>
        <header class="modal-header">
            <div class="tab-popup">
                <a class="tab-link tab-item tab-active" data-tab="tab-popup-1">Đăng nhập</a>
                <a class="tab-link tab-item" data-tab="tab-popup-2">Tạo tài khoản</a>
            </div>
        </header>
        <div class="modal-body">
            <div id="tab-popup-1" class="tab-content-popup tab-popup-current">
                <form role="form" method="POST" action="">
                    <div class="form-group row"><label for="uname" class="col-md-4 col-form-label text-md-right">Tài
                            khoản</label>
                        <div class="col-md-8">
                            <input type="text" id="uname" name="uname" placeholder="Tài khoản" autocapitalize="off"
                                class="form-control" require>
                            <label for="uname" id="uname_error" style="color: red;"></label>
                        </div>
                    </div>
                    <div class="form-group row"><label for="pass" class="col-md-4 col-form-label text-md-right">Mật
                            khẩu</label>
                        <div class="col-md-8">
                            <input type="password" id="pass" name="pass" placeholder="Nhập mật khẩu"
                                class="form-control" require>
                            <label for="pass" id="pass_error" style="color: red;"></label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-8 offset-md-4">
                            <div class="form-check custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="rememberme">
                                <label class="custom-control-label" for="rememberme">Nhớ mật khẩu</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mb-12">
                        <div class="col-md-10 offset-md-2">
                            <button type="submit" name="userlogin" onclick="return checkuserlogin();"
                                class="btn btn-custom">Đăng nhập</button>
                            <a href="#" class="ml-4 forgot-pass">Quên mật khẩu?</a></div>
                    </div>
                </form>
            </div>
            <div id="tab-popup-2" class="tab-content-popup ">
                <form role="form" method="POST" action="">
                    <div class="form-group row"><label for="fullname" class="col-md-4 col-form-label text-md-right">Họ
                            Tên</label>
                        <div class="col-md-8">
                            <input type="text" id="fullname" name="fullname" placeholder="Nhập họ tên"
                                autocapitalize="off" class="form-control" value="<?= isset($_POST['fullname']) ? $_POST['fullname'] : '';?>">
                            <label for="fullname" id="fullname_error" style="color: red;"></label>
                        </div>
                    </div>
                    <div class="form-group row"><label for="username" class="col-md-4 col-form-label text-md-right">Tên
                            tài khoản</label>
                        <div class="col-md-8">
                            <input type="text" id="username" name="username" placeholder="Nhập tên tài khoản"
                                autocapitalize="off" class="form-control" value="<?= isset($_POST['username']) ? $_POST['username'] : '';?>">
                            <label for="username" id="username_error" style="color: red;"><?= isset($error_signup) ? $error_signup : ''; ?></label>
                        </div>
                    </div>
                    <div class="form-group row"><label for="email"
                            class="col-md-4 col-form-label text-md-right">Email</label>
                        <div class="col-md-8">
                            <input type="email" id="email" name="email" placeholder="Nhập email" autocapitalize="off"
                                class="form-control" value="<?= isset($_POST['email']) ? $_POST['email'] : '';?>">
                            <label for="email" id="email_error" style="color: red;"></label>
                        </div>
                    </div>
                    <div class="form-group row"><label for="password" class="col-md-4 col-form-label text-md-right">Mật
                            khẩu</label>
                        <div class="col-md-8">
                            <input type="password" id="password" name="password" placeholder="Mật khẩu trên 8 ký tự"
                                class="form-control">
                            <label for="password" id="password_error" style="color: red;"></label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="repassword" class="col-md-4 col-form-label text-md-right">Nhập
                            lại mật khẩu</label>
                        <div class="col-md-8">
                            <input type="password" id="repassword" name="repassword" placeholder="Mật khẩu trên 8 ký tự"
                                class="form-control">
                            <label for="repassword" id="repassword_error" style="color: red;"></label>
                        </div>
                    </div>
                    <div class="form-group row"><label for="gender" class="col-md-4 col-form-label text-md-right">Giới
                            tính</label>
                        <div class="col-md-8">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline1" name="gender" class="custom-control-input"
                                    value="1" <?= (isset($_POST['gender']) == 1) ? "checked" : ""; ?> >
                                <label class="custom-control-label" for="customRadioInline1">Nam</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline2" name="gender" class="custom-control-input"
                                    value="0" <?= (isset($_POST['gender']) == 0) ? "checked" : ""; ?> >
                                <label class="custom-control-label" for="customRadioInline2">Nữ</label>
                                <label for="gender" id="gender_error" style="color: red;"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row"><label for="phone" class="col-md-4 col-form-label text-md-right">Số
                            điện thoại</label>
                        <div class="col-md-8">
                            <input type="text" id="phone" name="phone" placeholder="Nhập số điện thoại"
                                class="form-control" value="<?= isset($_POST['phone']) ? $_POST['phone'] : '';?>">
                            <label for="phone" id="phone_error" style="color: red;"></label>
                        </div>
                    </div>

                    <div class="form-group row"><label for="address" class="col-md-4 col-form-label text-md-right">Địa
                            chỉ</label>
                        <div class="col-md-8">
                            <input type="text" id="address" name="address" placeholder="Địa chỉ cụ thể"
                                class="form-control" value="<?= isset($_POST['address']) ? $_POST['address'] : '';?>">
                            <label for="address" id="address_error" style="color: red;"></label>
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-5 offset-md-7">
                            <button type="submit" name="signup" onclick="return checkusersignup();" class="btn btn-custom">Đăng ký tài khoản</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </article>
</div>

<script type="text/javascript">
function checkusersignup() {
    

    $('#username_error').hide();
    $('#password_error').hide();
    $('#fullname_error').hide();
    $('#email_error').hide();
    $('#repassword_error').hide();
    $('#phone_error').hide();
    $('#address_error').hide();

    var username_error = false;
    var password_error = false;
    var fullname_error = false;
    var email_error = false;
    var repassword_error = false;
    var phone_error = false;
    var address_error = false;

    if ($('#fullname').val() == '') {
        $('#fullname_error').html('Họ tên rỗng !');
        $('#fullname_error').show();
        fullname_error = true;
        return false;
    } else if ($('#username').val() == '') {
        $('#username_error').html('Tên đăng nhập rỗng !');
        $('#username_error').show();
        username_error = true;
        return false;
    } else if ($('#email').val() == '' || !validateEmail($('#email').val())) {
        $('#email_error').html('Email không hợp lệ !');
        $('#email_error').show();
        email_error = true;
        return false;
    } else if ($('#password').val() == '') {
        $('#password_error').html('Mật khẩu rỗng !');
        $('#password_error').show();
        password_error = true;
        return false;
    } else if ($('#repassword').val() == '' || $('#repassword').val() != $('#password').val()) {
        $('#repassword_error').html('Xác nhận mật khẩu không đúng !');
        $('#repassword_error').show();
        repassword_error = true;
        return false;
    } if ($('#phone').val() == '' || ($('#phone').val().length > 12) || !validatePhone($('#phone').val())) {
        $('#phone_error').html('Số điện thoại không hợp lệ !');
        $('#phone_error').show();
        phone_error = true;
        return false;
    } if ($('#address').val() == '') {
        $('#address_error').html('Địa chỉ rỗng !');
        $('#address_error').show();
        address_error = true;
        return false;
    } 
    return true;
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function validatePhone(phone) {
    var intRegex = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
    return intRegex.test(phone);
}
</script>
<script type="text/javascript">
    function checkuserlogin() {
        $('#uname_error').hide();
        $('#pass_error').hide();
        var uanme_error = false;
        var pass_error = false;

        if($('#uname').val() == '') {
            $('#uname_error').html('Tên đăng nhập rỗng !');
            $('#uname_error').show();
            uname_error = true;
            return false;
        } else if($('#pass').val() == '') {
            $('#pass_error').html('Mật khẩu rỗng !');
            $('#pass_error').show();
            pass_error = true;
            return false;
        }

        return true;
    }
</script>
