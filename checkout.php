<?php require_once("intc/header.php"); ?>
<?php if(!isset($_SESSION['cart'])) { ?>
<div class="breadcrumb-wrap">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="<?= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]" ?>">Trang chủ</a></li>
            <li><span>Giỏ hàng</span></li>
        </ul>
    </div>
</div> <!-- end breadcrumb -->

<div class="container" style="height: 340px;">
<?php if(isset($_SESSION['success'])) :?>
    <h2 style="text-align: center; color: #5fba7d;"><?= $_SESSION['success']; unset($_SESSION['success']); ?></h2>
<?php else: ?>
    <h2 style="text-align: center;">Giỏ hàng của bạn không có gì hết :( </h2>
    <h3 style="text-align: center;">Tìm và mua sản phẩm ngay nào các đồng chí <a href="<?= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]" ?>">Trang chủ</a></h3>
<?php endif; ?>
<?php } else { ?>

<div class="container" style="padding: 10px 0px">
        <form action="database/process.php" method="post" id="formdathang" name="formdathang">
            
        <?php if(isset($_SESSION['rm_sucess'])): ?>
            <div class="alert alert-success alert-dismissible fade show">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Thành công!</strong> <span style="font-weight: 400"><?= $_SESSION['rm_sucess']; ?></span>
            </div>
        <?php endif; unset($_SESSION['rm_sucess']); ?>

            <div id="summary-checkout">
                <div class="payment-info">
                    <h3>Thông tin thanh toán</h3>
                    <?php if(!isset($_COOKIE['login_userid'])): ?>
                    <div class="link-login">Bạn đã có tài khoản vui lòng <button type="button" data-modal-trigger="trigger-1" class="trigger" style="border: none; background:#fff;color:#4791d2;font-weight:bold">Đăng nhập</button></div>
                    <?php else: 
                        $userInfo = $db->fetchId("db_users", $_COOKIE['login_userid']);
                    endif; ?>
                    <div class="form_content">
                        <label>Họ và tên<span class="note-span">*</span> </label>
                        <input type="text" required="" placeholder="Họ tên của bạn để in hóa đơn" value="<?= (isset($userInfo['full_name'])) ? $userInfo['full_name'] : ''; ?>"
                            name="fullname">

                        <label>Email <span class="note-span">*</span> </label>
                        <input type="text" placeholder="Email của bạn (để nhận thông tin về đơn hàng)" value="<?= (isset($userInfo['email'])) ? $userInfo['email'] : ''; ?>"
                            name="email">

                        <label>Số điện thoại <span class="note-span">*</span> </label>
                        <input type="tel" placeholder="SĐT của bạn " value="<?= (isset($userInfo['phone'])) ? $userInfo['phone'] : ''; ?>" name="phone">

                        <label>Địa chỉ <span class="note-span">*</span></label>
                        <textarea placeholder="Số nhà, đường, phường, xã" name="addr" id="addr"
                            rows="3"><?= (isset($userInfo['address'])) ? $userInfo['address'] : ''; ?></textarea>

                        <label>Ghi chú</label>
                        <textarea placeholder="" name="note" id="note" rows="3"></textarea>
                    </div>
                </div>
                <div class="payment-way">
                    <h3>Hình thức thanh toán</h3>
                    <ul>
                        <li>
                            <label>
                                <input type="radio" value="Giao hàng thu tiền tận nơi" name="fpaymentmethod"
                                    class="radiopm" checked="">
                                <div class="pm-txt"> <i class="ic-delivery"></i>
                                    <h5> <a> Thanh toán khi nhận hàng COD</a></h5>
                                    <span>Bạn sẽ thanh toán tiền cho nhân viên giao hàng, sau khi nhận và kiểm tra hàng
                                        hóa</span>
                                </div>
                            </label>
                        </li>
                        <!-- <li>
                            <div class="space_5"></div>
                            <label style="width:100%">
                                <input type="radio" value="Chuyển khoản ngân hàng" name="fpaymentmethod"
                                    class="radiopm">
                                <div class="pm-txt"> <i class="ic-bank"></i>
                                    <h5><a> Chuyển khoản ngân hàng</a></h5>
                                </div>
                            </label>
                        </li> -->
                    </ul>
                </div>
                <div class="payment-confirm">
                    <h3>Xác nhận đơn hàng</h3>
                    <div class="cart-bg">
                    <?php
                        $num = 0; $total =0;
                        if(isset($_SESSION['cart'])):
                        foreach($_SESSION['cart'] as $key => $value):
                    ?>
                        <div class="r-row">
                            <div class="clearcart"><a href="../database/process.php?rmcart=<?= $key; ?>" title="Xóa">x</a></div>
                            <div class="tt-pro">
                                <a href="<?= $value['alias']; ?>-<?= $key; ?>.html" target="_blank"><?= $value['name']; ?></a>
                            </div>
                            <div class="image-co">
                                <a target="_blank" href="<?= $value['alias']; ?>-<?= $key; ?>.html">
                                    <img width="90" height="90" src="<?= $value['image']; ?>">
                                </a>
                            </div>
                            <div id="62263" class="txt-info">
                                <p class="number">Số lượng:
                                    <select name="quanty[0]" class="inputnumber cartquantity">
                                        <option value="1" selected=""><?= $value['qty']; ?></option>
                                    </select> × <?= bsVndDot($value['price']); ?> đ
                                </p>
                            </div>
                        </div>


                    <?php 
                        $total += $value['qty'];
                        $num += $value['price'] * $value['qty'];
                        $_SESSION['totalPrice'] = $num;
                        endforeach; endif; ?>
                    </div>
                    <div id="totalfeebox" style="clear: both; overflow:hidden" class="r-row"><br>
                        <h4 style="float: left;">THÀNH TIỀN:</h4>
                        <span style="float: right;" id="totalfee" class="red"><?= bsVndDot($num); ?> đ</span>
                    </div>
                    <div class="lofd-payment" style="overflow:hidden;">
                        <div style="clear: both;" id="shippingfeebox" class="r-row">
                            <h4 style="float: left;">PHÍ VẬN CHUYỂN:</h4>
                            <span style="float: right;" id="shippingfee" class="red">0đ</span>
                        </div>
                        <div class="lofd-payment r-row ">
                            <h4 style="float: left;">Tổng cộng:</h4>
                            <span id="totalfinalfees" style="float: right;" class="red1"><?= bsVndDot($num); ?> đ</span>
                        </div>
                    </div>
                    <?php if(!isset($_COOKIE['login_userid'])): ?>
                    <input type="submit" value="Đặt mua" class="btn-pay submitform" id="btncheckout" name="btncheckout" disabled style='background: gray;' >
                    <div><span style="color: red"><b>Bạn cần đăng nhập để đặt hàng !</b></span></div>
                    <?php else: ?>
                    <input type="submit" value="Đặt mua" class="btn-pay submitform" id="btncheckout" name="btncheckout" >
                    <div class="cont-shopping"><a href="<?= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]" ?>">Thêm sản phẩm khác</a></div>
                    <?php endif; ?>
                    
                    <input type="hidden" name="total" id="totlacart" class="totlacart" value="<?= bsVndDot($num); ?>">
                </div>
            </div>
        </form>
<?php } ?>
    </div>


<?php require_once("intc/footer.php"); ?>