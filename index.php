<?php require_once("intc/header.php"); ?>

    <div class="container">

        <div class="home_adv">
            <div class="row containerCarousel">
                <div id="home-carousel" class="owl-carousel homeCarousel">
                    <?php $slide = $db->fetchAll("db_slides"); ?>
                    <?php foreach($slide as $item): ?>
                    <div class="slide">
                        <a href="<?= $item['link'] ?>">
                            <img src="<?= $item['images']; ?>" alt="<?= $item['name']; ?>" class="img-responsive" />
                        </a>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <div class="clear-both"></div>
        <div class="space_10"></div>

        <div class="home-tab">
            <nav class="tabs">
                <div class="selector"></div>
                <a href="#" class="tab-link active current" data-tab="tab-1">HOT</a>
                <a href="#" class="tab-link" data-tab="tab-2">MỚI</a>
                <a href="#" class="tab-link" data-tab="tab-3">MUA NHIỀU</a>
                <a href="#" class="tab-link" data-tab="tab-4">XEM NHIỀU</a>
            </nav>

            <div id="tab-1" class="tab-content current">
                <?php $result = $db->fetchSql("SELECT `id`, `name`, `alias`, `images`, `price`, `sale_price` FROM `db_products` WHERE `hot` = 1 AND `trash` = 0 AND `status` = 1 LIMIT 0,4"); ?>
                <?php foreach($result as $item):?>
                <div class="items">
                    <div class="images">
                        <a href="<?= $item['alias']; ?>-<?= $item['id']; ?>.html">
                            <img class="lazy images-infade" data-original="<?= $item['images']; ?>" src="<?= $item['images']; ?>"
                                width="100%" />
                        </a>
                    </div>
                    <div class="info">
                        <h3><a href="<?= $item['alias']; ?>-<?= $item['id']; ?>.html"><?= $item['name']; ?></a></h3>
                        <p class="price">
                            <?php if($item['sale_price'] == 0): ?>
                                <span class="priceold"><?= bsVndDot($item['price']); ?> ₫</span>
                            <?php else: ?>
                                <span class="priceold"><?= bsVndDot($item['price']); ?> ₫</span> <span class="pricespecial"><?= bsVndDot($item['sale_price']); ?> ₫</span>
                            <?php endif; ?>
                        </p>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <div id="tab-2" class="tab-content">
                <?php $result = $db->fetchSql("SELECT `id`, `name`, `alias`, `images`, `price`, `sale_price` FROM `db_products` WHERE `new` = 1 AND `trash` = 0 AND `status` = 1 LIMIT 0,4"); ?>
                <?php foreach($result as $item): ?>
                <div class="items">
                    <div class="images">
                        <a href="<?= $item['alias']; ?>-<?= $item['id']; ?>.html">
                            <img class="lazy images-infade" data-original="<?= $item['images']; ?>" src="<?= $item['images']; ?>"
                                width="100%" />
                        </a>
                    </div>
                    <div class="info">
                        <h3><a href="<?= $item['alias']; ?>-<?= $item['id']; ?>.html"><?= $item['name']; ?></a></h3>
                        <p class="price">
                        <?php if($item['sale_price'] == 0): ?>
                            <span class="priceold"><?= bsVndDot($item['price']); ?> ₫</span>
                        <?php else: ?>
                            <span class="priceold"><?= bsVndDot($item['price']); ?> ₫</span> <span class="pricespecial"><?= bsVndDot($item['sale_price']); ?> ₫</span>
                        <?php endif; ?>
                        </p>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <div id="tab-3" class="tab-content">
                <?php $result = $db->fetchSql("SELECT `id`, `name`, `alias`, `images`, `price`, `sale_price` FROM `db_products` WHERE `trash` = 0 AND `status` = 1 ORDER BY `buyed` DESC LIMIT 0,4"); ?>
                <?php foreach($result as $item): ?>
                <div class="items">
                    <div class="images">
                        <a href="<?= $item['alias']; ?>-<?= $item['id']; ?>.html">
                            <img class="lazy images-infade" data-original="<?= $item['images']; ?>" src="<?= $item['images']; ?>"
                                width="100%" />
                        </a>
                    </div>
                    <div class="info">
                        <h3><a href="<?= $item['alias']; ?>-<?= $item['id']; ?>.html"><?= $item['name']; ?></a></h3>
                        <p class="price">
                        <?php if($item['sale_price'] == 0): ?>
                            <span class="priceold"><?= bsVndDot($item['price']); ?> ₫</span>
                        <?php else: ?>
                            <span class="priceold"><?= bsVndDot($item['price']); ?> ₫</span> <span class="pricespecial"><?= bsVndDot($item['sale_price']); ?> ₫</span>
                        <?php endif; ?>
                        </p>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <div id="tab-4" class="tab-content">
                <?php $result = $db->fetchSql("SELECT `id`, `name`, `alias`, `images`, `price`, `sale_price` FROM `db_products` WHERE `trash` = 0 AND `status` = 1 ORDER BY `view_count` DESC LIMIT 0,4"); ?>
                <?php foreach($result as $item): ?>
                <div class="items">
                    <div class="images">
                        <a href="<?= $item['alias']; ?>-<?= $item['id']; ?>.html">
                            <img class="lazy images-infade" data-original="<?= $item['images']; ?>" src="<?= $item['images']; ?>"
                                width="100%" />
                        </a>
                    </div>
                    <div class="info">
                        <h3><a href="<?= $item['alias']; ?>-<?= $item['id']; ?>.html"><?= $item['name']; ?></a></h3>
                        <p class="price">
                        <?php if($item['sale_price'] == 0): ?>
                            <span class="priceold"><?= bsVndDot($item['price']); ?> ₫</span>
                        <?php else: ?>
                            <span class="priceold"><?= bsVndDot($item['price']); ?> ₫</span> <span class="pricespecial"><?= bsVndDot($item['sale_price']); ?> ₫</span>
                        <?php endif; ?>
                        </p>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="clear-both"></div>
        <div class="space_10"></div>

        <?php $catpro = $db->fetchSql("SELECT id, name FROM db_categories WHERE parent_id = 0 ORDER BY name DESC"); ?>
        <?php if($catpro): ?>
        <?php foreach($catpro as $row): ?>
        
        <div class="tib_home blue">
            <span class="l-tib"><?= $row['name']; ?></span>
            <span class="total-tib"><a href="#">Tất cả</a></span>
        </div>
        <div class="box-pro">
            <?php $pro = $db->fetchSql("SELECT p.id, p.name, p.alias, images, price, sale_price FROM db_products as p INNER JOIN db_categories as c ON p.category_id = c.id WHERE p.category_id = {$row['id']} OR c.parent_id = {$row['id']} ORDER BY p.id LIMIT 0,4"); ?>
            <?php foreach($pro as $item): ?>
            <div class="items">
                <div class="images">
                    <a href="<?= $item['alias']; ?>-<?= $item['id']; ?>.html">
                        <img class="lazy images-infade" data-original="<?= $item['images']; ?>" src="<?= $item['images']; ?>"
                            width="100%" />
                    </a>
                </div>
                <div class="info">
                    <h3><a href="<?= $item['alias']; ?>-<?= $item['id']; ?>.html"><?= $item['name']; ?></a></h3>
                    <p class="price">
                    <?php if($item['sale_price'] == 0): ?>
                        <span class="priceold"><?= bsVndDot($item['price']); ?> ₫</span>
                    <?php else: ?>
                        <span class="priceold"><?= bsVndDot($item['price']); ?> ₫</span> <span class="pricespecial"><?= bsVndDot($item['sale_price']); ?> ₫</span>
                    <?php endif; ?>
                    </p>
                </div>
            </div>
            <?php endforeach; ?>
        </div>

        <div class="space_10"></div>
        <div class="clear"></div>
        
        <?php endforeach; ?>
        <?php endif; ?>
    </div>

<?php require_once("intc/footer.php"); ?>    