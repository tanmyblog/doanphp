<?php require_once("intc/header.php"); ?>
<?php 
    if(isset($_GET['catId']) && isset($_GET['keyword'])){
        $keyword = mysqli_real_escape_string($db->conn, $_GET['keyword']);
        $catId = $_GET['catId'];

        $allProducts = $db->fetchSql("SELECT * FROM `db_products` WHERE `name` LIKE '%{$keyword}%' "); 
        $catList = $db->fetchSql("SELECT `id`, `name`, `alias` FROM `db_categories` WHERE `parent_id` = 0 ORDER BY `name`"); 
        $total_records = count($allProducts);
        $current_page = isset($_GET['page']) ? $_GET['page'] : 1;
        $limit = 6;
        $total_page = ceil($total_records / $limit);
        if ($current_page > $total_page){
            $current_page = $total_page;
        }
        else if ($current_page < 1){
            $current_page = 1;
        }
        $start = ($current_page - 1) * $limit;
    } else {
        header('location: 404.php');
    }

 ?>
<div class="breadcrumb-wrap">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="/">Trang chủ</a></li>
            <?php $catname = $db->fetchId("db_categories", $catId); ?>
            <li><span><?= $keyword ?></span></li>
        </ul>
    </div>
</div>
<div class="container">

    <div class="side-bar">
        <div class="box-left-wrap visible-lg-block" id="boxleft-wrap">
            <div class="panel-group box-left" id="boxleft" role="tablist" aria-multiselectable="true">
                <div class="panel panel-info">
                    <div class="panel-heading" role="tab" id="heading-category">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" href="#collapse-category"
                                aria-controls="collapse-category">
                                Danh Mục Sản Phẩm
                            </a>
                        </h4>
                    </div>
                    <div id="collapse-category" class="panel-collapse " role="tabpanel"
                        aria-labelledby="heading-category">
                        <div class="list-group">
                            <?php foreach($catlist as $item){ ?>
                                <div class="list-group-item is-top">
                                    <a href="category.php?alias=<?= $item['alias'] ?>&catId=<?php echo $item['id']; ?>">
                                        <span><?php echo $item['name']; ?></span>
                                    </a>
                                </div>
                            <?php } ?>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="product-listing">
        <div class="product-box">
            <div class="option-box-wrap">
                <div class="option-box">
                    <div class="btn-group pull-right sort-box">
                        <span>Ưu tiên xem :</span>
                        <ul class="sort-list">
                            <li class=""><a href="javascript:void(0);">Hàng mới</a></li>
                            <li class=""><a href="javascript:void(0);">Bán chạy</a></li>
                            <li class=""><a href="javascript:void(0);">Giảm giá</a></li>
                            <li class=""><a href="javascript:void(0);">Giá thấp</a></li>
                            <li class=""><a href="javascript:void(0);">Giá cao</a></li>
                        </ul>
                    </div>
                </div>
            </div> <!-- end option box wrap -->

            <div class="box-pro">
                <?php if($allProducts) {
                foreach ($allProducts as $item) {  ?>
                    <div class="items">
                        <div class="images">
                            <a href="detail.php?alias=<?= $item['alias']; ?>&id=<?= $item['id']; ?>">
                                <img class="lazy images-infade" data-original="<?php echo $item['images']; ?>"
                                    src="<?php echo $item['images']; ?>" width="100%" />
                            </a>
                        </div>
                        <div class="info">
                            <h3><a href="detail.php?alias=<?= $item['alias']; ?>&id=<?= $item['id']; ?>"><?php echo $item['name']; ?></a></h3>
                            <p class="price">
                                <span class="priceold"><?php echo $item['price']; ?> ₫</span> <span class="pricespecial"><?php echo $item['sale_price']; ?> ₫</span>
                            </p>
                        </div>
                    </div> 
                <?php } }else {
                    echo "<b>Không tìm thấy sản phẩm có từ khóa <span style='color: red'>".$keyword. " </span>  </b>";
                } ?>

                
                
            </div> <!-- end box pro -->
        </div>
<?php require_once 'database/pagination.php'; ?>
        <div class="list-pager">
            <ul>
                <?php 
                if ($current_page > 1 && $total_page > 1){
                    echo '<li><a href="?keyword='.$keyword.'&catId='. $catId . '&page='.($current_page-1).'"><i class="fas fa-angle-left"></i></a></li> | ';
                } 
                for ($i=1; $i <= $total_page; $i++) { 
                    if($i == $current_page){
                        echo '<li><span class="current">'. $i .'</span></li>';
                    }else{
                        echo '<li><a class="normal" href="?keyword='.$keyword.'&catId='. $catId . '&page=' .$i. '">' .$i. '</a> </li>';
                    }
                }
                if ($current_page < $total_page && $total_page > 1){
                    // echo '<a href="index.php?page='.($current_page+1).'">Next</a> | ';
                    echo '<li><a rel="next" class="next" href="?keyword='.$keyword.'&catId='. $catId . '&page='.($current_page+1).'"><i class="fas fa-angle-right"></i></a></li>';
                }
                ?>          
            </ul>
        </div> <!-- end pagingation -->

    </div>
</div>

<?php require_once("intc/footer.php"); ?>