<?php require_once('intc/header.php'); ?>
<?php 
    include('../database/database.php');
    include(__DIR__.'/helper/notification.php');

    $db = new Database();
    $sql = "SELECT *  FROM `db_admins`";
    $result = $db->fetchSql($sql);

?>
        <!--sidebar start-->
        <?php require_once('intc/sidebar.php'); ?>
        <!--sidebar end-->

        <!--main content start-->
        <section id="main-content">
            <section class="wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header"><i class="fa fa fa-bars"></i> Pages</h3>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-home"></i><a href="/">Trang chủ</a></li>
                            <li><i class="fa fa-bars"></i>Quản lý tài khoản</li>
                        </ol>
                    </div>
                </div>
                <!-- page start-->

                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <!-- <header class="panel-heading">
                                
                            </header> -->

                            <table class="table table-striped table-advance table-hover">
                                <tbody>
                                    <tr>
                                        <th> ID</th>
                                        <th> Username</th>
                                        <th> Password</th>
                                        <th> Fullname</th>
                                        <th> Avatar</th>
                                        <th> Email</th>
                                        <th> Note</th>
                                        <th> Status</th>
                                        <th> Actions</th>
                                    </tr>
                                    <?php
                                    foreach ($result as $item):
                                ?>
                                    <tr>
                                        <td><?= $item['id']; ?></td>
                                        <td><?= $item['user_name']; ?></td>
                                        <td><?= $item['pass_word']; ?></td>
                                        <td><?= $item['full_name']; ?></td>
                                        <td> 
                                            <img src="img/<?= $item['avatar'] ?>" alt="" width="50">
                                        </td>
                                        <td><?= $item['email']; ?></td>
                                        <td><?= $item['note']; ?></td>    

                                        <td><?= ($item['status'] == 1) ? "<span style='color: #4cd964;font-weight:blod'>Bật</span>" : "<span style='color: #ff2d55;font-weight:blod'>Khóa</span>"; ?>
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn btn-success" href="accountedit.php?id=<?php echo $item['id']; ?>"><i class="icon_pencil-edit"></i></a>
                                                <a class="btn btn-danger" onClick="return confirm('Bạn có chắc xóa <?php echo $item['name']; ?> không ?');" href="../database/process.php?ac=delete&id=<?php echo $item['id']; ?>"><i class="icon_close_alt2"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </section>
                    </div>
                </div>
                <!-- page end-->
            </section>
        </section>

<?php require_once('intc/footer.php'); ?>