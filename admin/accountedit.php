<?php require_once('intc/header.php'); ?>
<?php 
    include('../database/database.php');
    include(__DIR__.'/helper/notification.php');

    $db = new Database();
    if(isset($_GET['id'])) {
        $result = $db->fetchId("db_admins", $_GET['id']);
    }
?>
<!--sidebar start-->
<?php require_once('intc/sidebar.php'); ?>
<!--sidebar end-->

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i> Pages</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="index.php">Trang chủ</a></li>
                    <li><a href="categorylist.php">Tài khoản</a></li>
                    <li>Thêm tài khoản</li>
                </ol>
            </div>
        </div>
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Thông tin tài khoản cần thêm
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <form class="form-validate form-horizontal " method="post" action="../database/process.php">
                                <div class="form-group ">
                                    <label for="name" class="control-label col-lg-2">Tên tài khoản <span
                                            class="required">*</span></label>
                                    <div class="col-lg-10">
                                        <input class=" form-control" id="username" name="username" type="text" value="<?= $result['user_name']; ?>" />
                                        <label for="username" id="name_error" class="error" style="color:red!important;"></label>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="parent_id" class="control-label col-lg-2">Mật khẩu <span
                                            class="required">*</span></label>
                                    <div class="col-lg-10">
                                        <input class=" form-control" id="password" name="password" type="password" />
                                        <label for="password" id="name_error" class="error" style="color:red!important;"></label>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="meta_keywords" class="control-label col-lg-2">Họ và tên </label>
                                    <div class="col-lg-10">
                                        <input class="form-control " id="fullname" name="fullname"
                                            type="text" value="<?= $result['full_name']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="avata" class="control-label col-lg-2">Avatar </label>
                                    <div class="col-lg-10">
                                        <input class="form-control " id="avata" name="avata" type="file" />
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="email" class="control-label col-lg-2">Email </label>
                                    <div class="col-lg-10">
                                        <input class="form-control " id="email" name="email" type="text" value="<?= $result['email']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="note" class="control-label col-lg-2 col-sm-3">Ghi chú </label>
                                    <div class="col-lg-10">
                                        <input class="form-control " id="note" name="note" type="text" value="<?= $result['note']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="sort" class="control-label col-lg-2 col-sm-3">Thứ tự </label>
                                    <div class="col-lg-10">
                                        <input class="form-control " id="sort" name="sort" type="text" value="<?= $result['sort']; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input type="hidden" name="id" value="<?= $result['id']; ?>">
                                        <button class="btn btn-primary" type="submit" name="editUser" onclick="return myfunction()">Thêm</button>
                                        <a href="account.php" class="btn btn-default">Trở
                                            về</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<script type="text/javascript">
    function myfunction() {
        $('#name_error').hide();
        var name_error = false;

        if($('#name').val() == '') {
            $('#name_error').html('Tên danh mục không được rỗng !');
            $('#name_error').show();
            name_error = true;
            return false;
        } else {
            $('#name_error').hide();
            return true;
        }
    }

</script>

<?php require_once('intc/footer.php'); ?>