<?php 
  session_start();
  if(isset($_SESSION['userid']) && $_SESSION['userid'] != ""){
    header("location:index.php");
  }
  if(isset($_POST['submit'])){
    $username = isset($_POST['username'])? $_POST['username'] : "";
    $password = isset($_POST['password'])? $_POST['password'] : "";

    include('../database/database.php');
    include(__DIR__.'/helper/notification.php');
    $db = new Database();
    $query = "user_name = '" .$username. "' AND pass_word = md5('" .$password. "')";
    if($result = $db->fetchOne("db_admins",$query)){
        $_SESSION['userid'] = $result['id'];
        $_SESSION['username'] = $result['user_name'];
      header("location:index.php");
    }else{ 
      $error = "Tài khoản hoặc mật khẩu không chính xác";
    }
    
  }
 ?>
<!DOCTYPE html>
<html lang="vi">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Trang quản trị website bán lan và phụ kiện">
  <meta name="author" content="Anh Khôi, Tấn Mỹ">
  <meta name="keyword" content="quản trị web bán lan">
  <link rel="shortcut icon" href="img/favicon.png">

  <title>Đăng nhập trang quản trị website bán lan và phụ kiện</title>

  <!-- Bootstrap CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="css/elegant-icons-style.css" rel="stylesheet" />
  <link href="css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet" />

</head>

<body class="login-img3-body">

  <div class="container">

    <form class="login-form" action="" method="post" name="submit">
      <div class="login-wrap">
        <div class="error">
          <span style="color:red; text-align: center; width: 100%; display: block;">
            <?php 
            if(isset($error) && $error != ""){
              echo $error;
            }
           ?></span>
          
        </div>
        <p class="login-img"><i class="icon_lock_alt"></i></p>
        <div class="input-group">
          <span class="input-group-addon"><i class="icon_profile"></i></span>
          <input type="text" name="username" class="form-control" placeholder="Username" autofocus>
        </div>
        <div class="input-group">
          <span class="input-group-addon"><i class="icon_key_alt"></i></span>
          <input type="password" name="password" class="form-control" placeholder="Password">
        </div>
        <label class="checkbox">
                <input type="checkbox" value="remember-me"> Remember me
                <span class="pull-right"> <a href="#"> Forgot Password?</a></span>
            </label>
        <button class="btn btn-primary btn-lg btn-block" name="submit" type="submit">Login</button>
      </div>
    </form>

  </div>


</body>

</html>
