<?php require_once('intc/header.php'); ?>
<?php
    include('../database/database.php');
    include(__DIR__.'/helper/common_helper.php');

    $db = new Database();
    if($_SERVER['REQUEST_METHOD'] == "POST") {
        $id = $_POST['slideid'];
        if (isset($_FILES['uf']) && $_FILES['uf']['name'] != '') {
            $target="../upload/slide/";
            $filename=basename($_FILES['uf']['name']);
            $target.=$filename;
            $link="upload/slide/".$filename;
            if (file_exists($target)) {
                echo "<script>alert('Ảnh đã tồn tại');</script>";
            }

            if (preg_match("/\.(jpg|png|bmp|gif)$/i", basename($_FILES['uf']['name']))) {
                if (move_uploaded_file($_FILES['uf']['tmp_name'], $target)) {
                    $data = array(
                        'name'          => $_POST['name'],
                        'description'   => $_POST['description'],
                        'images'        => $link,
                        'link'          => $_POST['link'],
                        'info'          => $_POST['info'],
                        'sort'          => $_POST['sort'],
                        'status'        => isset($_POST['status']) ? $_POST['status'] : 0
                    );
                    $result = $db->update("db_slides",$data, array('id' => $id));
                    if($result) {
                        header('location: slidelist.php');
                    }else {
                        echo "Error ".mysqli_error($db);
                        exit();
                    }
                }
            }
        } else {
            $data = array(
                'name'          => $_POST['name'],
                'description'   => $_POST['description'],
                'link'          => $_POST['link'],
                'info'          => $_POST['info'],
                'sort'          => $_POST['sort'],
                'status'        => isset($_POST['status']) ? $_POST['status'] : 0
            );
            $result = $db->update("db_slides",$data, array('id' => $id));
            if($result) {
                header('location: slidelist.php');
            }else {
                echo "Error ".mysqli_error($db);
                exit();
            }
        }
        
    }
?>
<!--sidebar start-->
<?php require_once('intc/sidebar.php'); ?>
<!--sidebar end-->

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i> Thêm slide</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="index.php">Trang chủ</a></li>
                    <li><a href="slidelist.php">Danh sách slide</a></li>
                    <li>Cập nhật slide</li>
                </ol>
            </div>
        </div>
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Thông tin Slide cần cập nhật
                    </header>
                    <div class="panel-body">
                        <div class="form">
                        <?php

                            $result = $db->fetchID("db_slides",$_GET['id']);
                        ?>
                            <form class="form-validate form-horizontal " method="post" enctype="multipart/form-data">
                                <div class="form-group ">
                                    <label for="name" class="control-label col-lg-2">Tên <span
                                            class="required">*</span></label>
                                    <div class="col-lg-10">
                                        <input class=" form-control" id="name" name="name" type="text" value="<?= $result['name'] ?>" require />
                                        <label for="name" id="name_error" class="error" style="color:red!important;"><?= isset($error_exists) ? $error_exists : ''; ?></label>
                                    </div>
                                </div>
                                <!-- anh dai dien -->
                                <div class="form-group ">
                                    <label for="images" class="control-label col-lg-2">Hình ảnh </label>
                                    <div class="col-lg-10">
                                        <img src="../<?= $result['images']; ?>" width="100">
                                        <input id="uf" name="uf" type="file" />
                                        <input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
                                        <label for="uf" id="uf_error" class="error" style="color:red!important;"></label>
                                    </div>
                                </div>
                                <!-- description -->
                                <div class="form-group ">
                                    <label for="description" class="control-label col-lg-2">Mô tả</label>
                                    <div class="col-lg-10">
                                        <textarea class="form-control" id="description" name="description"><?= $result['description'] ?></textarea>
                                    </div>
                                </div>
                                 <!-- link -->
                                 <div class="form-group ">
                                    <label for="link" class="control-label col-lg-2">Liên kết </label>
                                    <div class="col-lg-10">
                                        <textarea class="form-control" id="link" name="link" placeholder="Mô tả ngắn cho sản phẩm"><?= $result['link'] ?></textarea>
                                    </div>
                                </div>
                                <!-- 	info -->
                                <div class="form-group ">
                                    <label for="info" class="control-label col-lg-2">Ghi chú thêm </label>
                                    <div class="col-lg-10">
                                        <input class="form-control " id="info" name="info" type="text" value="<?= $result['info'] ?>" />
                                    </div>
                                </div>
                                <!-- sort -->
                                <div class="form-group ">
                                    <label for="sort" class="control-label col-lg-2">Thứ tự </label>
                                    <div class="col-lg-10">
                                        <input class="form-control " id="sort" name="sort" type="number" value="<?= $result['sort'] ?>" min="0"/>
                                    </div>
                                </div>
                                <!-- status -->
                                <div class="form-group ">
                                    <label for="status" class="control-label col-lg-2 col-sm-3">Trạng thái </label>
                                    <div class="col-lg-10 col-sm-9">
                                        <input type="checkbox" style="width: 20px" class="checkbox form-control"
                                            id="status" name="status" value="1" <?= (isset($result['status']) == 1 ) ? "checked" : ""; ?> />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input type="hidden" name="slideid" value="<?= $result['id']; ?>">
                                        <button class="btn btn-primary" type="submit" name="addSlide" onclick="return myfunction()">Cập nhật</button>
                                        <a href="slidelist.php" class="btn btn-default">Trở
                                            về</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<script type="text/javascript">
    function myfunction() {
        $('#name_error').hide();
        var name_error = false;

        if($('#name').val() == '') {
            $('#name_error').html('Tên không được rỗng !');
            $('#name_error').show();
            name_error = true;
            return false;
        }
        return true;
    }
</script>

<?php require_once('intc/footer.php'); ?>