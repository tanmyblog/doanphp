<?php require_once('intc/header.php'); ?>
<?php
    include('../database/database.php');
    include(__DIR__.'/helper/common_helper.php');

    $db = new Database();
    if($_SERVER['REQUEST_METHOD'] == "POST") {
        $id = $_POST['distId'];
        $data = array(
            'provin_id' => $_POST['provin_id'],
            'name'  => $_POST['name'],
            'sort'  => $_POST['sort'],
            'status'=> isset($_POST['status']) ? $_POST['status'] : 0
        );
        $result = $db->update('db_districts', $data, array('id' => $id));
        if($result) {
            header('location: districtlist.php');
        } else {
            echo "Error "._debug(mysqli_error($db));
            exit();
        }
    }
?>
<!--sidebar start-->
<?php require_once('intc/sidebar.php'); ?>
<!--sidebar end-->

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i> Cập nhật quận huyện</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="index.php">Trang chủ</a></li>
                    <li><a href="districtlist.php">Danh sách quận quyện</a></li>
                    <li>Cập nhật quận huyện</li>
                </ol>
            </div>
        </div>
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Thông tin tỉnh thành cần cập nhật
                    </header>
                    <div class="panel-body">
                        <div class="form">
                        <?php

                            $result = $db->fetchID("db_districts",$_GET['id']);
                        ?>
                            <form class="form-validate form-horizontal " method="post" enctype="multipart/form-data">
                                <!-- Tên -->
                                <div class="form-group ">
                                    <label for="name" class="control-label col-lg-2">Tên <span
                                            class="required">*</span></label>
                                    <div class="col-lg-10">
                                        <input class=" form-control" id="name" name="name" type="text" value="<?= $result['name'] ?>" require />
                                        <label for="name" id="name_error" class="error" style="color:red!important;"><?= isset($error_exists) ? $error_exists : ''; ?></label>
                                    </div>
                                </div>

                                <!-- tỉnh - thành phố -->
                                <div class="form-group ">
                                    <label for="provin_id" class="control-label col-lg-2">Tỉnh Thành Phố <span
                                            class="required">*</span></label>
                                    <div class="col-lg-10">
                                        <select class="form-control m-bot15" _autocheck="true" name="provin_id"
                                            id="provin_id">
                                            <option value="-1">Chọn tỉnh thành phố</option>
                                            <?php $list = $db->fetchSql("SELECT `id`, `name` FROM `db_provinces` ORDER BY `name`"); ?>
                                            <?php foreach($list as $row): ?>
                                            <option value="<?php echo $row['id']; ?>" <?php echo ($row['id'] == $result['provin_id']) ? 'selected' : ''; ?>><?php echo $row['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <label for="name" id="provin_error" class="error" style="color:red!important;"></label>
                                    </div>
                                </div>
                                
                                <!-- sort -->
                                <div class="form-group ">
                                    <label for="sort" class="control-label col-lg-2">Thứ tự </label>
                                    <div class="col-lg-10">
                                        <input class="form-control " id="sort" name="sort" type="number" value="<?= $result['sort'] ?>" min="0"/>
                                    </div>
                                </div>

                                <!-- status -->
                                <div class="form-group ">
                                    <label for="status" class="control-label col-lg-2 col-sm-3">Trạng thái </label>
                                    <div class="col-lg-10 col-sm-9">
                                        <input type="checkbox" style="width: 20px" class="checkbox form-control"
                                            id="status" name="status" value="1" <?php echo ($result['status']==1) ? "checked" : ""; ?>  />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input type="hidden" name="distId" value="<?= $result['id']; ?>">
                                        <button class="btn btn-primary" type="submit" name="addSlide" onclick="return checkFunction()">Cập nhật</button>
                                        <a href="districtlist.php" class="btn btn-default">Trở về</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<script type="text/javascript">
function checkFunction() {
    $('#name_error').hide();
    $('#provin_error').hide();
    var name_error = false;
    var provin_error = false;

    if ($('#name').val() == '') {
        $('#name_error').html('Tên không được rỗng !');
        $('#name_error').show();
        name_error = true;
        return false;
    } else if($('#provin_id').val() == -1) {
        $('#provin_error').html('Chưa chọn tỉnh / thành phố !');
        $('#provin_error').show();
        provin_error = true;
        return false;
    }
    return true;
}
<?php require_once('intc/footer.php'); ?>