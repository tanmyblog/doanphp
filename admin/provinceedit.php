<?php require_once('intc/header.php'); ?>
<?php
    include('../database/database.php');
    include(__DIR__.'/helper/common_helper.php');

    $db = new Database();
    if($_SERVER['REQUEST_METHOD'] == "POST") {
        $id = $_POST['provinId'];
        $data = array(
            'name'          => $_POST['name'],
            'sort'          => $_POST['sort']
        );
        $result = $db->update('db_provinces', $data, array('id' => $id));
        if($result) {
            header('location: provincelist.php');
        } else {
            echo "Error ".mysqli_error($db);
            exit();
        }
    }
?>
<!--sidebar start-->
<?php require_once('intc/sidebar.php'); ?>
<!--sidebar end-->

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i> Cập nhật tình thành - quận huyện</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="index.php">Trang chủ</a></li>
                    <li><a href="provincelist.php">Danh sách tỉnh thành - quận quyện</a></li>
                    <li>Cập nhật tình thành - quận huyện</li>
                </ol>
            </div>
        </div>
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Thông tin tỉnh thành cần cập nhật
                    </header>
                    <div class="panel-body">
                        <div class="form">
                        <?php

                            $result = $db->fetchID("db_provinces",$_GET['id']);
                        ?>
                            <form class="form-validate form-horizontal " method="post" enctype="multipart/form-data">
                                <div class="form-group ">
                                    <label for="name" class="control-label col-lg-2">Tên <span
                                            class="required">*</span></label>
                                    <div class="col-lg-10">
                                        <input class=" form-control" id="name" name="name" type="text" value="<?= $result['name'] ?>" require />
                                        <label for="name" id="name_error" class="error" style="color:red!important;"><?= isset($error_exists) ? $error_exists : ''; ?></label>
                                    </div>
                                </div>
                                
                                <!-- sort -->
                                <div class="form-group ">
                                    <label for="sort" class="control-label col-lg-2">Thứ tự </label>
                                    <div class="col-lg-10">
                                        <input class="form-control " id="sort" name="sort" type="number" value="<?= $result['sort'] ?>" min="0"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input type="hidden" name="provinId" value="<?= $result['id']; ?>">
                                        <button class="btn btn-primary" type="submit" name="addSlide" onclick="return checkFunction()">Cập nhật</button>
                                        <a href="provincelist.php" class="btn btn-default">Trở về</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<script type="text/javascript">
function checkFunction() {
    $('#name_error').hide();
    var name_error = false;

    if ($('#name').val() == '') {
        $('#name_error').html('Tên không được rỗng !');
        $('#name_error').show();
        name_error = true;
        return false;
    }
    return true;
}
</script>
<?php require_once('intc/footer.php'); ?>