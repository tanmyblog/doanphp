<?php require_once('intc/header.php'); ?>
<?php
    include('../database/database.php');
    include(__DIR__.'/helper/common_helper.php');
    include(__DIR__.'/helper/notification.php');

    $db = new Database();

    //  get page
    if (isset($_GET['p'])) {
        $p = $_GET['p'];
    } else {
        $p=1;
    }

    $sql = "SELECT * FROM `db_provinces` ORDER BY `id` DESC ";
    $total = count($db->fetchSql($sql));
    $result = $db->fetchJones("db_provinces", $sql, $total, $p, 10, true);

    $pageCount = $result['page'];
    unset($result['page']);

?>
<!--sidebar start-->
<?php require_once('intc/sidebar.php'); ?>
<!--sidebar end-->

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i> Danh sách tỉnh thành phố</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="/">Trang chủ</a></li>
                    <li>Danh sách tỉnh thành phố</li>
                </ol>
            </div>
        </div>
        <!-- page start-->

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <!-- <header class="panel-heading">
                                
                            </header> -->
                    <table class="table table-striped table-advance table-hover">
                        <tbody>
                            <tr>
                                <th> ID</th>
                                <th> Tên Tỉnh / Thành Phố</th>
                                <th> Thứ tự</th>
                                <th> Vận chuyển</th>
                                <th> Trạng thái</th>
                                <th> Action</th>
                            </tr>
                            <?php
                                    foreach ($result as $item):
                                ?>
                            <tr>
                                <td><?= $item['id']; ?></td>
                                <td><?= $item['name']; ?></td>
                                <td><?= $item['sort']; ?></td>
                                <td><?= $item['ship']; ?></td>
                                <td><?= ($item['status'] == 1) ? "<span style='color: #4cd964;font-weight:blod'>Bật</span>" : "<span style='color: #ff2d55;font-weight:blod'>Khóa</span>"; ?>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-primary" data-toggle="modal" href="#modalAdd" > <i
                                                class="icon_plus_alt2"> </i> </a>
                                        <a class="btn btn-success" href="provinceedit.php?id=<?= $item['id']; ?>"> <i class="icon_check_alt2"> </i> </a>
                                        <a class="btn btn-danger" id="<?= $item['id']; ?>"
                                            onClick="return confirm('Bạn có chắc xóa <?php echo $item['name']; ?> không ?');"
                                            href="../database/process.php?provinDel=<?= $item['id']; ?>"><i
                                                class="icon_close_alt2"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </section>
                <?php if ($pageCount >= 2): ?>
                <section class="panel">
                    <div class="text-center">
                        <ul class="pagination pagination-sm">
                            <?php for ($i=1; $i <= $pageCount; $i++): ?>
                            <li><a href="?p=<?= $i ?>"><?= $i; ?></a></li>
                            <?php endfor; ?>
                        </ul>
                    </div>
                    <?php
                        // $path = $_SERVER['SCRIPT_NAME'];
                        // _debug($path);
                    ?>
                </section>
                <?php endif; ?>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>

<!-- Modal -->
<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="../database/process.php" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Thêm tỉnh / thành phố mới</h4>
                </div>
                <div class="modal-body">
                    <!-- name -->
                    <div class="form-group " style="padding: 15px 0;">
                        <label for="name" class="control-label col-lg-2">Tên <span style="color: red">*</span></label>
                        <div class="col-lg-10">
                            <input class=" form-control" id="name" name="name" type="text" require />
                            <label for="name" id="name_error" class="error" style="color:red!important;"></label>
                        </div>
                    </div>

                    <!-- sort -->
                    <div class="form-group " style="padding: 15px 0;">
                        <label for="sort" class="control-label col-lg-2">Thứ tự </label>
                        <div class="col-lg-10">
                            <input class=" form-control" id="sort" name="sort" type="text" require />
                        </div>
                    </div>

                    <!-- status -->
                    <div class="form-group " style="padding: 20px 0;">
                        <label for="status" class="control-label col-lg-2 col-sm-3">Trạng thái </label>
                        <div class="col-lg-10 ">
                            <input type="checkbox" style="width: 20px" class="checkbox form-control"
                                id="status" name="status" value="1" checked />
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="display: block; margin-top: 20px;">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Đóng</button>
                    <button class="btn btn-success" type="submit" name="addProvin"
                        onclick="return checkFunction();">Lưu</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- modal -->

<script type="text/javascript">
function checkFunction() {
    $('#name_error').hide();
    var name_error = false;

    if ($('#name').val() == '') {
        $('#name_error').html('Tên không được rỗng !');
        $('#name_error').show();
        name_error = true;
        return false;
    }
    return true;
}
</script>
<?php require_once('intc/footer.php'); ?>