<?php require_once('intc/header.php'); ?>
<?php
    include('../database/database.php');
    include(__DIR__.'/helper/common_helper.php');

    $db = new Database();
    if(!isset($_GET['id'])) {
        header('location: productlist.php');
    } else {
        $id = $_GET['id'];
        $sql = " `id`= {$id} ";
        $result = $db->fetchOne("db_products",$sql);   
    }
?>
<!--sidebar start-->
<?php require_once('intc/sidebar.php'); ?>
<!--sidebar end-->

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i> Pages</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="index.php">Trang chủ</a></li>
                    <li><a href="productlist.php">Danh sách sản phẩm</a></li>
                    <li>Cập nhật sản phẩm</li>
                </ol>
            </div>
        </div>
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Thông tin danh mục cần thêm
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <form class="form-validate form-horizontal " method="post" action="../database/process.php" enctype="multipart/form-data">
                                <div class="form-group ">
                                    <label for="name" class="control-label col-lg-2">Tên sản phẩm <span
                                            class="required">*</span></label>
                                    <div class="col-lg-10">
                                        <input class=" form-control" id="name" name="name" type="text" value="<?= isset($result['name']) ? $result['name'] : ''; ?>" require />
                                        <label for="name" id="name_error" class="error" style="color:red!important;"></label>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="product_code" class="control-label col-lg-2">Mã sản phẩm <span
                                            class="required">*</span></label>
                                    <div class="col-lg-10">
                                        <input class=" form-control" id="product_code" name="product_code" type="text" value="<?= isset($result['product_code']) ? $result['product_code'] : ''; ?>" require />
                                        <label for="name" id="product_code_error" class="error" style="color:red!important;"></label>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="category_id" class="control-label col-lg-2">Danh mục sản phẩm <span
                                            class="required">*</span></label>
                                    <div class="col-lg-10">
                                        <select class="form-control m-bot15" _autocheck="true" name="category_id"
                                            id="category_id">
                                            <option value="-1">Chọn danh mục</option>
                                            <?php $list = $db->fetchSql("SELECT `id`, `name` FROM `db_categories` WHERE `parent_id`=0 "); ?>
                                                <?php foreach($list as $row): ?>
                                                <?php $sub = $db->fetchSql("SELECT `id`, `name` FROM `db_categories` WHERE `parent_id`= {$row['id']} "); ?>
                                                <option value="<?php echo $row['id']; ?>" <?php echo ($row['id'] == $result['category_id']) ? 'selected' : ''; ?> style="font-weight: bold; color: #4791d2"><?php echo $row['name']; ?></option>
                                                <?php if(!empty($sub)):
                                                    foreach($sub as $item):
                                                ?>
                                                    <option value="<?php echo $item['id']; ?>" <?php echo ($item['id'] == $result['category_id']) ? 'selected' : ''; ?> style="margin-left: 10px;"> -- <?php echo $item['name']; ?></option>
                                                <?php endforeach; endif; ?>
                                            <?php endforeach; ?>
                                        </select>
                                        <label for="name" id="category_error" class="error" style="color:red!important;"></label>
                                    </div>
                                </div>
                                <!-- anh dai dien -->
                                <div class="form-group ">
                                    <label for="images" class="control-label col-lg-2">Ảnh đại diện </label>
                                    <div class="col-lg-10">
                                        <img src="../<?= $result['images']; ?>" width="100">
                                        <input id="uf" name="uf" type="file" />
				                        <input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
                                    </div>
                                </div>
                                <!-- anh khac -->
                                <!-- <div class="form-group ">
                                    <label for="more_images" class="control-label col-lg-2">Các ảnh khác </label>
                                    <div class="col-lg-10">
                                        <input type="file" id="more_images" name="more_images[]" multiple>
                                        <span style="color: red; font-weight: 400; font-size: 14px;"></span>
                                    </div>
                                </div> -->
                                <!-- content -->
                                <div class="form-group ">
                                    <label for="content" class="control-label col-lg-2">Nội dung</label>
                                    <div class="col-lg-10">
                                        <textarea class="form-control" id="content" name="editor1"><?= isset($result['contents']) ? $result['contents'] : ''; ?></textarea>
                                    </div>
                                </div>
                                 <!-- shot_descriptions -->
                                 <div class="form-group ">
                                    <label for="shot_descriptions" class="control-label col-lg-2">Mô tả ngắn </label>
                                    <div class="col-lg-10">
                                        <textarea class="form-control" id="shot_descriptions" name="shot_descriptions" placeholder="Mô tả ngắn cho sản phẩm"><?= isset($result['shot_descriptions']) ? $result['shot_descriptions'] : ''; ?></textarea>
                                    </div>
                                </div>
                                <!-- meta_keywords -->
                                <div class="form-group ">
                                    <label for="meta_keywords" class="control-label col-lg-2">Từ khóa seo </label>
                                    <div class="col-lg-10">
                                        <input class="form-control " id="meta_keywords" name="meta_keywords" placeholder="Cách nhau đấu phẩy" value="<?= isset($result['meta_keywords']) ? $result['meta_keywords'] : ''; ?>"
                                            type="text" />
                                    </div>
                                </div>
                                <!-- meta_descriptions -->
                                <div class="form-group ">
                                    <label for="meta_descriptions" class="control-label col-lg-2">Mô tả seo </label>
                                    <div class="col-lg-10">
                                        <textarea class="form-control" id="meta_descriptions" name="meta_descriptions" placeholder="Mô tả seo không quá 160 ký tự"><?= isset($result['meta_descriptions']) ? $result['meta_descriptions'] : ''; ?></textarea>
                                    </div>
                                </div>
                                <!-- price -->
                                <div class="form-group ">
                                    <label for="price" class="control-label col-lg-2">Giá bán </label>
                                    <div class="col-lg-10">
                                        <input class="form-control " id="price" name="price" type="text" value="<?= isset($result['price']) ? bsVndDot($result['price']) : ''; ?>" onkeyup="this.value = FormatNumber(this.value);"/>
                                        <label for="price" id="price_error" class="error" style="color:red!important;"></label>
                                    </div>
                                </div>
                                <!-- sale price -->
                                <div class="form-group ">
                                    <label for="sale_price" class="control-label col-lg-2">Giá giảm </label>
                                    <div class="col-lg-10">
                                        <input class="form-control " id="sale_price" name="sale_price" type="text" value="<?= isset($result['sale_price']) ? bsVndDot($result['sale_price'])  : ''; ?>" onkeyup="this.value = FormatNumber(this.value);" />
                                    </div>
                                </div>
                                <!-- qty -->
                                <div class="form-group ">
                                    <label for="qty" class="control-label col-lg-2">Số lượng </label>
                                    <div class="col-lg-10">
                                        <input class="form-control " id="qty" name="qty" type="text" value="<?= isset($result['qty']) ? bsVndDot($result['qty']) : ''; ?>" onkeyup="this.value = FormatNumber(this.value);"/>
                                        <label for="name" id="qty_error" class="error" style="color:red!important;"></label>
                                    </div>
                                </div>
                                 <!-- hot, new,m status -->
                                 <div class="form-group ">
                                    <label for="hot" class="control-label col-lg-2 col-sm-3">Sản phẩm hót </label>
                                    <div class="col-lg-10 col-sm-9">
                                        <input type="checkbox" style="width: 20px" class="checkbox form-control"
                                            id="hot" name="hot" value="1" <?php echo ($result['hot']==1) ? "checked" : ""; ?> />
                                    </div>

                                    <label for="new" class="control-label col-lg-2 col-sm-3">Sản phẩm mới </label>
                                    <div class="col-lg-10 col-sm-9">
                                        <input type="checkbox" style="width: 20px" class="checkbox form-control"
                                            id="new" name="new" value="1" <?php echo ($result['new']==1) ? "checked" : ""; ?>  />
                                    </div>

                                    <label for="status" class="control-label col-lg-2 col-sm-3">Trạng thái </label>
                                    <div class="col-lg-10 col-sm-9">
                                        <input type="checkbox" style="width: 20px" class="checkbox form-control"
                                            id="status" name="status" value="1" <?php echo ($result['status']==1) ? "checked" : ""; ?>  />
                                    </div>
                                </div>
       
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <input type="hidden" name="idpro" value="<?= $result['id']; ?>">
                                        <input type="hidden" name="userid" value="<?= $_SESSION['userid']; ?>">
                                        <button class="btn btn-primary" type="submit" name="upPro" onclick="return myfunction()">Thêm</button>
                                        <a href="productlist.php" class="btn btn-default">Trở
                                            về</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<script type="text/javascript">
    function myfunction() {
        $('#name_error').hide();
        $('#product_code_error').hide();
        $('#category_error').hide();
        $('#price_error').hide();
        $('#qty_error').hide();

        var name_error = false;
        var product_code_error = false;
        var category_error = false;
        var price_error = false;
        var qty_error = false;

        if($('#name').val() == '') {
            $('#name_error').html('Tên danh mục không được rỗng !');
            $('#name_error').show();
            name_error = true;
            return false;
        }
        else if($('#product_code').val() == '') {
            $('#product_code_error').html('Mã sản phẩm không được rỗng !');
            $('#product_code_error').show();
            product_code_error = true;
            return false;
        } else if($('#category_id').val() == -1 ) {
            $('#category_error').html('Chưa chọn danh mục !');
            $('#category_error').show();
            category_error = true;
            return false;
        } else if($('#price').val() == '') {
            $('#price_error').html('Giá bán không được dể trống !');
            $('#price_error').show();
            price_error = true;
            return false;
        } else if($('#qty').val() == '' || $('#qty').val() == 0) {
            $('#qty_error').html('Số lượng không được dể trống !');
            $('#qty_error').show();
            price_error = true;
            return false;
        }

        return true;
    }

</script>
<script type="text/javascript">
    if (typeof CKEDITOR == 'undefined') {
        document.write('CKEditor');
    }else {
        var editorContent = CKEDITOR.replace('editor1'); 
    }
</script>  
<?php require_once('intc/footer.php'); ?>