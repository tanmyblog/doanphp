<?php require_once('intc/header.php'); ?>
<?php 
    include('../database/database.php');
    include(__DIR__.'/helper/notification.php');

    $db = new Database();
    if(!isset($_GET['id'])) {
        
        header('location: categorylist.php');
    } else {
        $id = $_GET['id'];
        $sql = " `id`= {$id} ";
        $result = $db->fetchOne("db_categories",$sql);    
    }
?>
<!--sidebar start-->
<?php require_once('intc/sidebar.php'); ?>
<!--sidebar end-->

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i> Pages</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="index.php">Trang chủ</a></li>
                    <li><a href="categorylist.php">Danh mục sản phẩm</a></li>
                    <li>Cập nhật danh mục</li>
                </ol>
            </div>
        </div>
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Thông tin danh mục cần cập nhật
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <form class="form-validate form-horizontal " method="post" action="../database/process.php">
                                <div class="form-group ">
                                    <label for="name" class="control-label col-lg-2">Tên danh mục <span
                                            class="required">*</span></label>
                                    <div class="col-lg-10">
                                        <input class=" form-control" id="name" name="name" type="text" value="<?= $result['name'];?>"/>
                                        <label for="name" id="name_error" class="error" style="color:red!important;"></label>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="parent_id" class="control-label col-lg-2">Danh mục cha <span
                                            class="required">*</span></label>
                                    <div class="col-lg-10">
                                        <select class="form-control m-bot15" _autocheck="true" name="parent_id" id="parent_id">
                                            <option value="0">Danh mục cha</option>
                                            <?php $list = $db->fetchSql("SELECT `id`, `name` FROM `db_categories` WHERE `parent_id`=0 "); ?>
                                                <?php foreach($list as $row): ?>
                                                <?php $sub = $db->fetchSql("SELECT `id`, `name` FROM `db_categories` WHERE `parent_id`= {$row['id']} "); ?>
                                                <option value="<?php echo $row['id']; ?>" <?php echo ($row['id'] == $result['parent_id']) ? 'selected' : ''; ?> style="font-weight: bold; color: #4791d2"><?php echo $row['name']; ?></option>
                                                <?php if(!empty($sub)):
                                                    foreach($sub as $item):
                                                ?>
                                                    <option value="<?php echo $item['id']; ?>" <?php echo ($item['id'] == $result['parent_id']) ? 'selected' : ''; ?> style="margin-left: 10px;"> -- <?php echo $item['name']; ?></option>
                                                <?php endforeach; endif; ?>
                                            <?php endforeach; ?>
                                        </select> 
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="meta_keywords" class="control-label col-lg-2">Từ khóa seo </label>
                                    <div class="col-lg-10">
                                        <input class="form-control " id="meta_keywords" name="meta_keywords"
                                            type="text" value="<?= $result['meta_keywords'];?>"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="meta_descriptions" class="control-label col-lg-2">Mô tả seo </label>
                                    <div class="col-lg-10">
                                        <input class="form-control " id="meta_descriptions" name="meta_descriptions"
                                            type="text" value="<?= $result['meta_descriptions'];?>"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="sort" class="control-label col-lg-2">Thứ tự </label>
                                    <div class="col-lg-10">
                                        <input class="form-control " id="sort" name="sort" type="number" value="<?= $result['sort'];?>"/>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="status" class="control-label col-lg-2 col-sm-3">Trạng thái </label>
                                    <div class="col-lg-10 col-sm-9">
                                        <input type="checkbox" style="width: 20px" class="checkbox form-control"
                                            id="status" name="status" value="1" <?php echo ($result['status']==1) ? "checked" : ""; ?> />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                    <input type="hidden" name="idcat" value="<?= $result['id']; ?>">
                                        <input type="hidden" name="userid" value="<?= $_SESSION['userid']; ?>">
                                        <button class="btn btn-primary" type="submit" name="upCat" onclick="return myfunction()">Cập nhật</button>
                                        <a href="categorylist.php" class="btn btn-default">Trở
                                            về</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</section>
<script type="text/javascript">
    function myfunction() {
        $('#name_error').hide();
        var name_error = false;

        if($('#name').val() == '') {
            $('#name_error').html('Tên danh mục không được rỗng !');
            $('#name_error').show();
            name_error = true;
            return false;
        } else {
            $('#name_error').hide();
            return true;
        }
    }

</script>

<?php require_once('intc/footer.php'); ?>