<?php require_once('intc/header.php'); ?>
<?php 
    include('../database/database.php');
    include(__DIR__.'/helper/common_helper.php');
    include(__DIR__.'/helper/notification.php');

    $db = new Database();

    //  get page
    if (isset($_GET['p'])) {
        $p = $_GET['p'];
    } else {
        $p=1;
    }

    $sql = "SELECT p.`id`, p.`product_code`, p.`name`, p.`images`, p.`price`, p.`sale_price`, p.`qty`, p.`buyed`, p.`created_date`, ad.`full_name` FROM `db_products` AS p INNER JOIN `db_admins` AS ad ON p.`created_by_user_id` = ad.`id` ORDER BY p.`id` DESC";
    $total = count($db->fetchSql($sql));
    $result = $db->fetchJones("db_products", $sql, $total, $p, 20, true);

    $pageCount = $result['page'];
    unset($result['page']);

    $list = $db->fetchSql("SELECT `id`, `name` FROM `db_categories` WHERE `parent_id`=0 ");
?>
        <!--sidebar start-->
        <?php require_once('intc/sidebar.php'); ?>
        <!--sidebar end-->

        <!--main content start-->
        <section id="main-content">
            <section class="wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header"><i class="fa fa fa-bars"></i> Pages</h3>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-home"></i><a href="/">Trang chủ</a></li>
                            <li>Quản lý sản phẩm</li>
                        </ol>
                    </div>
                </div>
                <!-- page start-->

                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <!-- <header class="panel-heading">
                                
                            </header> -->

                            <table class="table table-striped table-advance table-hover">
                                <tbody>
                                    <tr>
                                        <th> ID</th>
                                        <th> Sản phẩm</th>
                                        <th> Mã sản phẩm</th>
                                        <th> Giá bán</th>
                                        <th> Giá giảm</th>
                                        <th> SL</th>
                                        <th> Đã Bán</th>
                                        <th> Ngày đăng</th>
                                        <th> Người đăng</th>
                                        <th> Action</th>
                                    </tr>
                                    <?php
                                    foreach ($result as $item):
                                ?>
                                    <tr>
                                        <td><?= $item['id']; ?></td>
                                        <td>
                                            <img src="../<?= $item['images'] ?>" alt="" width="50">
                                            <?= $item['name']; ?>
                                        </td>
                                        <td><?= $item['product_code']; ?></td>
                                        <td><?= bsVndDot($item['price']); ?> đ</td>
                                        <td><?= (isset($item['sale_price'])) ? $item['sale_price'] : 0; ?> đ</td>
                                        <td><?= $item['qty']; ?></td>
                                        <td><?= $item['buyed']; ?></td>
                                        <td><?= date("d/m/Y", $item['created_date']); ?></td>
                                        <td><?= $item['full_name']; ?></td>
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn btn-primary" href="productadd.php"><i class="icon_plus_alt2"></i></a>
                                                <a class="btn btn-success" href="productedit.php?id=<?= $item['id']; ?>"><i class="icon_check_alt2"></i></a>
                                                <a class="btn btn-danger" onClick="return confirm('Bạn có chắc xóa <?php echo $item['name']; ?> không ?');" href="../database/process.php?proDel=<?= $item['id']; ?>"><i class="icon_close_alt2"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </section>
                        <?php if($pageCount >= 2): ?>
                            <section class="panel">
                                <div class="text-center">
                                    <ul class="pagination pagination-sm">
                                        <?php for ($i=1; $i <= $pageCount; $i++): ?>
                                        <li><a href="?p=<?= $i ?>"><?= $i; ?></a></li>
                                        <?php endfor; ?>
                                    </ul>
                                </div>
                                <?php
                                    // $path = $_SERVER['SCRIPT_NAME'];
                                    // _debug($path);
                                ?>
                            </section>
                        <?php endif; ?>
                    </div>
                </div>
                <!-- page end-->
            </section>
        </section>

<?php require_once('intc/footer.php'); ?>