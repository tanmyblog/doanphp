<?php require_once("intc/header.php");
    if(!isset($_COOKIE['login_userid'])) {
        header('location: 404.php');
    } else {
        $result = $db->fetchId("db_users", $_COOKIE['login_userid']);
        if(isset($_POST['uploadav'])) {
            if (isset($_FILES['uf'])) {
                $target="upload/avatar/";
                $filename=basename($_FILES['uf']['name']);
                $target.=$filename;
                $link="upload/avatar/".$filename;
                if (file_exists($target)) {
                    echo "<script>alert('Ảnh đã tồn tại');</script>";
                }
    
                if (preg_match("/\.(jpeg|jpg|png|bmp|gif)$/i", basename($_FILES['uf']['name']))) {
                    if (move_uploaded_file($_FILES['uf']['tmp_name'], $target)) {
                        $data = array('avatar' => $link);

                        if($db->update("db_users", $data, array('id'=>$_COOKIE['login_userid']))) {
                            header('location: profile.php');
                        } else {
                            echo "<script>alert(' Lỗi không cập nhật được ');</script>";
                        }
                    } echo "<script>alert(' Không upload được ');</script>";
                } echo "<script>alert(' file không đúng định dạng ');</script>";
            } else {
                echo "<script>alert('Không tồn lại file upload');</script>";
            }
        }
    }
?>
<div class="breadcrumb-wrap">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="/">Trang chủ</a></li>
            <li><span>Hồ sơ của <?= (isset($_COOKIE['login_userid'])) ? $_COOKIE['login_fullname'] : '';?></span></li>
        </ul>
    </div>
</div>
<div class="container">

    <div class="side-bar">
        <div class="box-avatar">
        <img src="<?php if(!isset($result['avatar']) || $result['avatar'] == ''){ 
            echo 'upload/avatar/noava.jpg';
        } else {
            echo $result['avatar'];
        } ?>" alt="<?= $_COOKIE['login_fullname']; ?>">
        </div>
        <div class="box-left-wrap visible-lg-block" id="boxleft-wrap">
            <div class="panel-group box-left" id="boxleft" role="tablist" aria-multiselectable="true">
                <div class="panel panel-info">
                    <div id="collapse-category" class="panel-collapse " role="tabpanel"
                        aria-labelledby="heading-category">
                        <div class="list-group">
                            <div class="list-group-item is-top">
                                <a href="ho-so"><span>Trang cá nhân</span></a>
                                <a href="lich-su-mua-hang"><span>Lịch sửa mua hàng</span></a>
                                <a href="cap-nhat-anh-dai-dien"><span>Cập nhật ảnh đại diện</span></a>
                                <a href="cap-nhat-thanh-vien"><span>Thiết lặp tài khoản</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="product-listing">
        <div class="product-box">
            <div class="option-box-wrap">
                <div class="option-box">
                    <span>Thay đổi ảnh đại diện</span>
                </div>
            </div> <!-- end option box wrap -->
            <div class="box-pro">
                <form method="post" action="" enctype="multipart/form-data">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="uf"><b>Chọn tiệp tin ảnh</b></label>
                            <input type="file" class="form-control-file" id="uf" name="uf">
                            <input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
                        </div>
                        <div class="form-group col-md-6">
                        <img width="150" height="150" src="<?php if(!isset($result['avatar']) || $result['avatar'] == ''){ 
                                echo 'upload/avatar/noava.jpg';
                            } else {
                                echo $result['avatar'];
                            } ?>" alt="<?= $_COOKIE['login_fullname']; ?>"> <br/><br/>
                            <label for="uploadav"><b>Ảnh hiện tại</b></label>
                        </div>
                    </div>
                    <button class="btn btn-primary" name="uploadav" type="submit">Cập Nhật</button>
                </form>

            </div>
        </div>
    </div>
</div>

<?php require_once("intc/footer.php"); ?>