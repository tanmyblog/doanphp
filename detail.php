<?php require_once("intc/header.php"); ?>
<?php
    if(!isset($_GET['id'])) {
        header('location: 404.php');
    } else {
        $result = $db->fetchId("db_products", $_GET['id']);
        if(!$result) { 
            header('location: 404.php');
        } else { 
            $breadCat = $db->fetchSql("SELECT c.id, c.name, c.alias, c.parent_id FROM db_products AS p INNER JOIN db_categories AS c WHERE c.id = '{$result['category_id']}' LIMIT 1");
        }        
    }

    if(isset($_POST['addtocart'])) {
        $id = $_POST['id'];
        $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if(!isset($_SESSION['cart'][$id]))
        {
            // tao moi gio hang
            $_SESSION['cart'][$id]['name'] = $result['name'];
            $_SESSION['cart'][$id]['alias'] = $result['alias'];
            $_SESSION['cart'][$id]['image'] = $result['images'];
            $_SESSION['cart'][$id]['price'] = $result['price'];
            $_SESSION['cart'][$id]['qty'] = (int)$_POST['qty'];
            header('location: '.$link);
            
        }
        else if (isset($_SESSION['cart'][$id]))
        {
            // cap nhat gio hang
            $_SESSION['cart'][$id]['qty'] += 1;
            header('location: '.$link);
        } 
    } else {}
?>
<div class="breadcrumb-wrap">
        <div class="container">
            <ul class="breadcrumbs">
                <li><a href="/">Trang chủ</a></li>
                <?php foreach($breadCat as $cat): ?>
                <li><a href="<?= $cat['alias']; ?>-<?= $cat['id']; ?>"><?= $cat['name']; ?></a></li>
                <?php endforeach; ?>
                <li><span><?= $result['name']; ?></span></li>
                
            </ul>
        </div>
    </div> <!-- end breadcrumb -->
    <?php // _debug($_SESSION['cart']); ?>
    <div class="container">
        <div class="top-detailt">
            <div class="image_pro">
                <div class="largimages">
                    <div>
                        <a href="#" data-lightbox="roadtrip" class="libox">
                            <img src="<?= $result['images'] ?>" id="idIMG" alt="<?= $result['name'] ?>"></a>
                    </div>
                </div>
                <!-- <ul id="etalage">
                
                    <li>
                        <a class="zoomThumbActive" href="javascript:void(0);"
                            rel="/data/Product/dong-ho-cap-hublot-cao-cap-1545022175.jpg">
                            <img class="etalage_thumb_image" src="../source/images/dong-ho2.jpg"
                                alt="Đồng hồ cặp Hublot cao cấp">
                        </a>
                    </li>

                </ul> -->
            </div> <!-- end image pro -->
            <div class="box-text-detail">
                <div class="box-left">
                    <h1 class="deal_detail_name_m text-uppercase"><?= $result['name'] ?></h1>
                    <div class="deal_detail_codepro">Mã sản phẩm: <b><?= $result['product_code'] ?></b></div>
                    <div class="detial_price">
                    <?php
                        if($result['sale_price'] != 0):
                    ?>
                        <b><?= bsVndDot($result['sale_price']); ?></b>
                        <span class="old_price">Giá gốc: <del><?= bsVndDot($result['price']); ?></del></span> </div>
                    <?php else: ?>
                        <b><?= bsVndDot($result['price']); ?></b>
                    <?php endif; ?>
                    <div class="div-description">
                        <b>Mô tả ngắn</b>
                        <p><?= $result['shot_descriptions']; ?></p>
                    </div>
                    <p class="kgach">Tình trạng: <?php if($result['qty'] > 0): ?><span style="color: #00b57d"><b>Còn hàng</b></span><?php else: ?><span style="color: red"><b>Hết hàng</b></span> &nbsp; <?php endif; ?> Tồn kho: <?= $result['qty']; ?></p>
                    <form action="" method="post">
                        <div class="form-group">
                            <input type="number" value="1" min="1" name="qty" id="qty " class="form-control" style="width: 60px!important" />
                        </div>
                        <input type="hidden" name="id" value="<?= $result['id']; ?>"/>
                        <input type="hidden" name="price" value="<?= ($result['sale_price'] > 0) ? $result['sale_price'] : $result['price']; ?>"/>
                        <button class="btn btn-success my-cart-btn" type="submit" name="addtocart" <?= ($result['qty'] <= 0) ? "disabled" : ""; ?> >Thêm vào giỏ</button>
                    </form>
                </div> <!-- end box-left -->

            </div> <!-- end box text detail -->

        </div>
<div class="con-wrap">
        <div class="side-bar">
            <div class="box-left-wrap visible-lg-block" id="boxleft-wrap">
                <div class="panel-group box-left" id="boxleft" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-info">
                        <div class="panel-heading" role="tab" id="heading-category">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" aria-controls="collapse-category"> Danh Mục Sản Phẩm </a>
                            </h4>
                        </div>
                        <div id="collapse-category"  aria-labelledby="heading-category">
                            <div class="list-group">
                                <?php $sql = $db->fetchSql("SELECT `id`, `name`, `alias` FROM `db_categories` WHERE `parent_id` = {$cat['parent_id']} ");?>
                                <?php foreach($sql as $catitem): ?>
                                <div class="list-group-item ">
                                    <a href="<?= $catitem['alias'];?>-<?= $catitem['id']; ?>">
                                        <?= ($catitem['id'] == $cat['id']) ? '<b>'.$catitem['name'].'</b>' : $catitem['name']; ?>
                                    </a>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="product-listing">
            <div class="product-box">
                <div class="option-box-wrap">
                    <div class="option-box">
                        <div class="btn-group pull-right sort-box">
                            <ul class="tab_nav sort-list">
                                <li class="tab-link current" data-tab="tab-prod-1"><a>Chi tiết sản phẩm</a></li>
                                <li class="tab-link" data-tab="tab-prod-2"><a>Đánh giá sản phẩm</a></li>
                            </ul>
                        </div>
                    </div>
                </div> <!-- end option box wrap -->

                <div class="template_content">
                    <div class="prod-content">
                        <div id="tab-prod-1" class="tab_content current">
                            <?= $result['contents']; ?>
                        </div>
                        <div id="tab-prod-2" class="tab_content">
                            <div class="comment-face">
                                <div id="fb-root"></div>
                                <script>
                                    (function (d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) return;
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src =
                                            'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=736899939977417&autoLogAppEvents=1';
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }(document, 'script', 'facebook-jssdk'));
                                </script>
                                <div class="fb-comments" data-href="<?= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" data-width="100%" data-numposts="5">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="space_10"></div>
                </div>

            </div> <!-- end product box -->
        </div> <!-- end product listing -->
        </div>
    </div>

    <div class="related-product">
        <div class="rp-wraper">
            <div class="related-title">
                <h2>Sản phẩm liên quan</h2>
            </div>
            <div class="box-pro">
            <?php $related = $db->fetchSql("SELECT `id`, `name`, `alias`, `images`, `price`, `sale_price` FROM `db_products` WHERE `id` != '{$result['id']}' AND `category_id` = '{$result['category_id']}'  ORDER BY RAND() DESC LIMIT 0,4 "); ?>
            <?php foreach($related as $item):?>
                <div class="items">
                    <div class="images">
                        <a href="<?= $item['alias']; ?>-<?= $item['id']; ?>.html">
                            <img class="lazy images-infade" data-original="<?= $item['images']; ?>" src="<?= $item['images']; ?>"
                                width="100%" />
                        </a>
                    </div>
                    <div class="info">
                        <h3><a href="<?= $item['alias']; ?>-<?= $item['id']; ?>.html"><?= $item['name']; ?></a></h3>
                        <p class="price">
                            <?php if($item['sale_price'] == 0): ?>
                                <span class="priceold"><?= bsVndDot($item['price']); ?> ₫</span>
                            <?php else: ?>
                                <span class="priceold"><?= bsVndDot($item['price']); ?> ₫</span> <span class="pricespecial"><?= bsVndDot($item['sale_price']); ?> ₫</span>
                            <?php endif; ?>
                        </p>
                    </div>
                </div>
                <?php endforeach; ?>
                
            </div> <!-- end box pro -->
        </div>

    </div> <!-- end related product -->

<?php require_once("intc/footer.php"); ?>    